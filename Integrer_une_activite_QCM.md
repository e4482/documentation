# Intégrer une activité QCM

L'activité **QCM** permet de créer un questionnaire à choix multiples. Les questions et réponses peuvent être des éléments textuels, multimédias ou formules mathématiques.

Cette activité est une activité d'entraînement, donc les élèves pourront la tenter autant de fois qu'ils le souhaitent.



## Étape 1 - Ajouter une activité QCM

Pour créer une activité **QCM**, ouvrez le parcours sur lequel vous souhaitez ajouter cette activité. 

Cliquez en haut à droite sur le bouton "**Activer le mode édition**".

Dans la barre d'édition tout en haut, cliquez sur le menu "**Activités**", puis sur l'icône correspondante à l'activité QCM.

![Appariement- Choix de l'activité](images/activite_QCM/Capture1.png)

Ou sélectionnez la section concernée puis :

- Cliquez sur **"Ajouter une activité ou une ressource"** ;

- Cliquez dans l'onglet **"Activité"**

- Puis, sélectionnez l'activité **Jeu d'appariement **dans la liste ;

![Appariement- Choix de l'activité](images/activite_QCM/Capture2.png)

Une nouvelle page s'ouvre pour définir les paramètres de l'activité. 

## Étape 2 - Ajouter du contenu dans votre activité QCM

Commencez par attribuer un nom à votre activité et, si vous le souhaitez, une petite description.

![Appariement-titre](images/activite_QCM/Capture3.png)

Complétez ensuite le texte de la question, puis les réponses. N'oubliez pas de cocher la ou les réponses exactes. Vous pouvez insérer des images en y associant une description pour les logiciels d’accessibilité, des ressources multimédias sons et vidéos (que vous pouvez déposer sur Peertube ou PodEduc d'Apps éducation s’ils sont volumineux). 

![Appariement-titre](images/activite_QCM/Capture4.png)

Vous pouvez ajouter des questions supplémentaires en cliquant sur "**Ajouter une question**".

- **Enregistrez et afficher** votre exercice.

## Étape 3 - Visualiser votre activité QCM

Vous avez la possibilité de visualiser votre **QCM**, en cliquant sur les réponses : sélectionneées, elles passent au vert. Cliquez sur le bouton "Vérifier la réponse" pour valider.

En cas de bonnes réponses, des coches apparaissent. Le score obtenu est de 100%.

![Appariement-titre](images/activite_QCM/Capture5.png)

En cas de mauvaises réponses, des croix apparaissent.

![Appariement-titre](images/activite_QCM/Capture6.png)

Une réponse partiellement correcte donne un score de 0 %.

![Appariement-titre](images/activite_QCM/Capture7.png)

## Étape 4 - Suivre l'activité des élèves

Plusieurs cas de figure dans le tableau de suivi :

![Modifier le jeu](images/activitemillionaire/Ts.png)

- L'élève n'a pas réalisé l'activité : par défaut, la case correspondante à l'activité est de couleur grise avec un rond noir et le chiffre 0 indique que l'élève n'a effectué aucune tentative.

![Modifier le jeu](images/activitemillionaire/KO.png)

- L'élève a réalisé au moins une tentative : par défaut, l'activité est considérée comme achevée. La case s'affiche en vert et le nombre dans le cercle indique le nombre de tentative(s).

![Modifier le jeu](images/activitemillionaire/OK2.png)

- Si vous souhaitez savoir si l'activité a été réussie (100% de réussite), vous devez ne laisser cocher dans les conditions d'achèvement que la case "L'étudiant doit recevoir une note pour achever cette activité".

![Modifier le jeu](images/activitemillionaire/Capture8.png)

Vous pourrez alors vérifier que l'activité a été réussie ou non après "x" tentative(s) grâce à la couleur de la case. Par exemple, dans le cas ou l'élève aurait tenté l'activité 1 fois en commettant des erreurs, la case restera grise avec le chiffre 1 à l'intérieur. En cas de réussite, la case passe au vert.

![Modifier le jeu](images/activitemillionaire/KO1.png)







