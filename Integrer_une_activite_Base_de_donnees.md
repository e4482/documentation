# Intégrer une activité Base de données

## Tutoriel pour le professeur
<https://tube-numerique-educatif.apps.education.fr/w/31ztoHtqbyrsCxiPYgH1Go>

## Tutoriel pour les élèves
<https://tube-numerique-educatif.apps.education.fr/w/qp4iAk1WGqXQzXT9zUQi3V>
