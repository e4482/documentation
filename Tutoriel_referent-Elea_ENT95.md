# Créer les comptes utilisateurs avec l'ENT 95

## Étape 1 - Exporter vos comptes utilisateurs depuis l'ENT

Connectez-vous à l'ENT avec les identifiants d'un compte ayant les pouvoirs d'administrateur pour votre établissement : https://cas.moncollege.valdoise.fr

Avant de continuer, vous aurez peut-être besoin de sélectionner votre établissement dans la liste déroulante qui apparaît en haut à droite de l'écran lorsque vous cliquez sur **Mes ENT**.

![](images/ent95/capture1.png)

Dans le menu de gauche, cliquez successivement sur **SERVICES ÉTABLISSEMENT** puis **Annuaire**.

![](images/ent95/capture2.png)

Un menu secondaire apparaît, cliquez sur **Administration** puis **Fichiers des identifiants**.

![](images/ent95/capture3.png)

À droite s'affiche à présent la liste des fichiers précédemment exportés. N'y prêtez pas attention et cliquez directement sur **Valider** pour générer un nouveau fichier.

![](images/ent95/capture4.png)

Le fichier le plus récent se trouve tout en haut du tableau...

![](images/ent95/capture5.png)

1. Téléchargez le premier fichier ![](images/ent95/capture6.png)
2. Supprimez-le de l'ENT ![](images/ent95/capture7.png)



> **Remarques : **
>
> Si votre navigateur vous demande de choisir entre **Ouvrir** et **Enregistrer** le fichier, vous devez choisir cette seconde solution.
>
> ![](images/ent95/capture8.png)
>
> Recherchez le fichier dans votre ordinateur et vérifiez qu'il se nomme bien **ENT-Identifiants-UAI.csv** où l'UAI est l'identifiant unique de votre établissement.
>
> Il ne doit y avoir aucun caractère supplémentaire entre l'UAI et l'extension du fichier. Si cela arrive, c'est que vous n'aviez pas supprimé un ou plusieurs fichiers précédemment exportés !

## Étape 2 - Se connecter à la plateforme Éléa de l'établissement

Un courriel automatique est adressé au chef d'établissement / IEN via l'adresse **UAI@ac-versailles.fr**.
Il contient le lien permettant **d'activer le compte local de gestion de l'établissement** sur la plateforme Éléa, puis d'y **importer les comptes utilisateurs.**

Entrez l'url correspondant à votre plateforme de bassin sur le modèle **BASSIN.elea.ac-versailles.fr**

![accueil_elea](images/accueil_elea/capture1.png)

Cliquez sur **« Utiliser mon compte local ».**

![accueil_elea](images/accueil_elea/capture2_91.png)

Puis entrez **l'UAI (RNE) et le mot de passe.**

![accueil_elea](images/accueil_elea/capture3.png)

## Étape 3 - Importer vos comptes utilisateurs dans Éléa

Sur la page d'accueil, cliquez sur **« Réaliser une nouvelle importation »**.

![import](images/import/capture9_91.png)

Puis cliquez sur **« Choisir un fichier »** ou **glissez/déposez** celui-ci.

![import](images/import/capture9.png)

Patientez pendant la création des cohortes et des utilisateurs. La procédure est automatique et ne demande aucune action de votre part.

![creation_cohortes](images/import/capture7.png)

Attendez l’affichage du message **« importation terminée ».**

![importation_terminée](images/import/capture10.png)
