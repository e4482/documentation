# Accéder aux parcours-ressources académiques

En suivant la procédure ci-dessous, vous allez **avoir accès à une dizaine de parcours-ressources académiques sur la plateforme de e-éducation Éléa**. Ces parcours ont été conçus par des enseignants et des formateurs de l'académie de Versailles dans le cadre d'un appel à projet lancé en juin 2016. 
Validés pédagogiquement par un ou plusieurs inspecteurs, ces productions permettent de découvrir les potentialités de la plateforme Éléa et la mise en oeuvre de différents scénarios pédagogiques (pédagogie inversée, pédagogie de projet, gamification, pédagogie différenciée, etc.).

**Chaque parcours est accessible en double** et vous permet une consultation en tant qu'"enseignant" ou en tant qu'"étudiant". Vous pouvez ainsi consulter la totalité d'un parcours en tant qu'enseignant lorsqu'il contient des restrictions d'accès prévues pour les élèves.

*Pré-requis* : posséder la clé d'inscription au groupe "Accès aux parcours-ressources".


## Étape 1 : accéder à la plateforme Éléa

Cliquez sur l'url suivante : https://communaute.elea.ac-versailles.fr/local/teacherboard/cohortkey.php 

Elle vous permet d'accéder directement à la plateforme Éléa et, une fois identifié.e, vous aurez accès aux parcours-ressources académiques.

## Étape 2 : se connecter à la plateforme Éléa

Cliquez sur **« Démarrer »**.

![page_accueil_elea](images/accueil_elea/capture1.png)

Sélectionnez **« Utiliser mon compte académique »**.

![choix_compte](images/accueil_elea/capture2.png)

Entrez votre identifiant académique et votre mot de passe (les mêmes que pour la messagerie académique). Puis cliquez sur **« Connexion »**.

![connexion_utilisateur](images/accueil_elea/capture3.png)


## Étape 3 : accéder aux parcours-ressources 

Saisissez la clé que l'on vous a communiquée pour avoir "Accès aux parcours-ressources" sur la plateforme Éléa.

![acces_cohorte](images/cohort_inspecteurs/cohort1.png)

Si la clé n'est pas correcte, un message d'erreur vous avertit et vous pouvez refaire une tentative. Pensez en particulier à respecter la casse (majuscules et minuscules).

![acces_cohorte](images/cohort_inspecteurs/cohort2.png)

Une fois la clé saisie, un message vous annonce votre intégration au groupe "Accès aux parcours-ressources". En cliquant sur le bouton **« Revenir à la page d'accueil »**, vous accédez automatiquement au tableau de bord de la plateforme Éléa.

![acces_cohorte](images/cohort_inspecteurs/cohort3.png)

## Étape 4 : consulter les parcours-ressources 

Sur le tableau de bord, vous pouvez découvrir des parcours exemples intitulés "PARCOURS-RESSOURCES_discipline(s)_niveau_titre (type d'accès)".

![acces_cohorte](images/cohort_inspecteurs/cohort4.png)

Pour vous connecter ultérieurement à la plateforme Éléa vous pouvez désormais utiliser l'url https://communaute.elea.ac-versailles.fr/ puis vous identifier, sans saisir à nouveau la clé d'inscription.

