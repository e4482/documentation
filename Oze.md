# Créer les comptes utilisateurs avec l'ENT Oze

<https://tube-numerique-educatif.apps.education.fr/w/eSKnHXboPJFwtoQJrqWfYH>

## Étape 1 - Exporter vos comptes utilisateurs depuis l'ENT

**Connectez-vous à votre ENT** avec un compte administrateur puis cliquez sur le menu bleu en haut à droite.

![Mes Oz'apps](images/ent_oze/menu.png)

Dans **« Mes Oz'Apps favorites »**, cliquez sur **« Annuaire »**.

![Mes Oz'apps](images/ent_oze/OzApps.png)

Cliquez sur **« AFFICHER TOUS LES UTILISATEURS »**.

![Profil](images/ent_oze/Annuaire_profil.png)

Puis sur **« Exporter »** et enfin sélectionnez **« SACoche(CSV) »**.

![Exporter](images/ent_oze/Export_SACoche.png)

Enregistrez le fichier.

![Enregistrer](images/ent_oze/Enregistrer_sous.png)

**Remarque** : Pour des questions juridiques, tous les fichiers exportés doivent être **détruits** dès la fin de la procédure.  


## Étape 2 - Se connecter à la plateforme Éléa de l'établissement

Un courriel automatique est adressé au chef d'établissement / IEN via l'adresse **UAI@ac-versailles.fr**.
Il contient le lien permettant d'activer le compte local de gestion de l'établissement sur la plateforme Éléa, puis d'y importer les comptes utilisateurs.

Entrez l'url correspondant à votre plateforme de bassin sur le modèle **BASSIN.elea.ac-versailles.fr**

![accueil_elea](images/accueil_elea/capture1.png)

Cliquez sur **« Utiliser mon compte local ».**

![accueil_elea](images/accueil_elea/capture2_78.png)

Puis entrez **l'UAI (RNE) et le mot de passe.**

![accueil_elea](images/accueil_elea/capture3.png)

## Étape 3 - Importer vos comptes utilisateurs dans Éléa

Cliquez sur **« NOUVELLE IMPORTATION »**.

![import](images/ent_oze/nouvelle_importation.png)

Cliquez sur **« Choisir un fichier »** ou **glissez/déposez** le fichier que vous avez enregistré à l'étape précédente.

![import](images/import/capture9.png)

Patientez pendant la création des cohortes et des utilisateurs. La procédure est automatique et ne demande aucune action de votre part.

![creation_cohortes](images/ent_oze/import_etape2.png)

![import](images/ent_oze/import_etape3.png)

![importation_terminée](images/ent_oze/import_etape4.png)

