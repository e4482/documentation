## Ajouter le contenu interactif

1. Activez le mode édition dans votre parcours en cliquant sur ce bouton en haut à droite.

2. Cliquez dans la section souhaitée de votre parcours sur " **Ajouter une activité et ressource** " pour y créer une activité du type "**Contenu Interactif**".

![Bouton H5P](images/creercontenusinteractifspartiecommune/creercontenuinteractif01activiteh5p.png)

Ou créez l'activité à partir de la banque de contenus.

![Bouton H5P](images/creercontenusinteractifspartiecommune/H5Pb.png)

3. Dans le formulaire qui s’affiche, après avoir saisi éventuellement une rapide description de l'activité qui va être créée, repérez le type de contenu interactif souhaité dans la section "**Éditeur**" et le menu "**H5P hub - Sélectionnez le type d'activité**" ; puis cliquez sur le bandeau correspondant pour créer la ressource voulue.

Si vous souhaitez davantage d'informations sur la procédure de création d'un contenu interactif, un tutoriel détaillé est disponible ici : [Tutoriel Créer un Contenu Interactif](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=integrer-des-contenus-interactifs&item=creer-un-contenu-interactif).
