# Intégrer une activité GeoGebra

## Créer une activité GeoGebra

Pour créer une activité GeoGebra, ouvrez le parcours sur lequel vous souhaitez ajouter une activité Geogebra, activez le mode édition, sélectionnez la section concernée puis :

- cliquez sur **"Ajouter une activité ou une ressource"**,
- passez en **"Mode Avancé"**,
- sélectionnez l'activité **"GeoGebra" dans la liste**,
- et enfin cliquez sur **"Ajouter"**.      

![Choix de l'activité](images\activite_geogebra\geogebra.png) 

Une nouvelle page s'ouvre pour définir les paramètres de l'activité. 

## Paramétrer l'activité

Commencez par attribuer un nom à votre activité et si vous le souhaitez une petite description.

### Ajouter un contenu

Dans l'onglet **"Contenu"**, 2 possibilités s'offrent à vous pour ajouter un fichier .ggb, selon **"le type"** sélectionné :

- à partir d'un fichier local via le sélecteur de fichiers ou par glisser/déposer dans la zone de dépôt,

  ![Choix de l'activité](images\activite_geogebra\fichier_ggb.png)
  
- via une URL externe (elle doit pointer vers un fichier ggb).

  ![Choix de l'activité](images\activite_geogebra\url_ggb.png)
  

Vous pouvez également sélectionner le **"Langage"** (français ou anglais) 


### Choisir les paramètres d'affichage

Dans l'onglet **"Apparence"**

- Ajustez l’affichage de votre fichier en définissant la largeur et la hauteur (diminuer pour zoomer, augmenter pour dézoomer)

- Cliquez sur **"Afficher plus …"** pour faire apparaître des menus complémentaires

  ![Choix de l'activité](images\activite_geogebra\afficher_plus.png)

  Vous pouvez alors ajouter :

  - ajouter des fonctionnalités,

  - définir l'affichage des barres dans l'interface utilisateur.

    ![Choix de l'activité](images\activite_geogebra\dans_afficher_plus.png)

### Autres paramètres
Comme pour toutes les activités, il convient de définir les réglages courants, l'achèvement d'activité ou encore les restrictions d'accès selon vos besoins.

Terminez en cliquant sur **"Enregistrer et revenir au cours"**ou **"Enregistrer et afficher"**.




## Récupérer les résultats et les figures des élèves

Pour accéder aux résultats des élèves, ouvrez l'activité GeoGebra concernée, puis cliquez sur le menu bleu en haut à droite, puis sur "Administration de GeoGebra" et enfin "Résultats".

Un tableau contenant toutes les informations s'ouvre :

![Choix de l'activité](images\activite_geogebra\tableau.png)

En cliquant sur les liens "Note", tout à droite, vous pouvez obtenir la figure de chaque élève.

## Activités auto-évaluées

Il est possible de proposer des exercices interactifs notés à partir d'un fichier Geogebra interactif, c'est-à-dire contenant une variable "***grade***" comprise entre 0 et la note maximale définie dans votre activité GeoGebra sur Eléa.

Au moment de la création de l'activité GeoGebra, après avoir ajouté votre contenu, il convient de renseigner la partie **"Note" **en respectant les consignes ci-après :

![Choix de l'activité](images\activite_geogebra\notes.png)



**Quelques exemples d'activités auto-évaluées**

- https://www.geogebra.org/m/d4GKJwmq

- http://lycee-valin.fr/maths/exercices_en_ligne/moodle.html

## Obtenir l'url du fichier .ggb sur le site https://www.geogebra.org/

Voici les étapes à suivre :

- Sur le site https://www.geogebra.org/, afficher l'activité qui vous intéresse
- Cliquer sur les trois points en haut à droite puis **Infos** : ![](images/activite_geogebra/infos.png)
- Cliquer sur **Télécharger** et cocher la case pour **accepter les conditions** : ![](images/activite_geogebra/conditions.png)
- Enfin, faire **clic droit** puis **copier l'adresse du lien** (ce lien pointera vers l'url du fichier .ggb et sera utilisable comme URL externe dans ELEA)


## Quelques ressources complémentaires

Voici une liste (non exhaustive) de sites où l'on peut trouver des exemples de figures créées avec GeoGebra :

- https://www.geogebra.org/materials : de nombreuses ressources pour la classe que l'on peut utiliser, modifier et enregistrer au format .ggb (Menu - Ouvrir avec l'application GeoGebra - Menu - Exporter en - ggb)
- https://www.geogebra.org/m/jmh9kbbp : collection d'animations pour illustrer des cours de mathématiques du collège et lycée (Figures conçues pour les programmes Lumni sur France4 dans le cadre du programme #NationApprenante lors de l'épisode de COVID19)
- Le site "m@ths et tiques" :
  - activités pour le collège : https://www.maths-et-tiques.fr/index.php/tp-info/outils-pour-la-classe
  - activités pour le lycée : https://www.maths-et-tiques.fr/index.php/tp-info/outils-pour-la-classe-au-lycee
- Exemples de réalisations et fiches techniques pour des mathématiques dynamiques : http://www.univ-irem.fr/lexique/co/site.html
- Animations et applications constructives : http://dmentrard.free.fr/GEOGEBRA/Maths/accueilmath.htm
- Exemples d'activités avec GeoGebra 3D : http://www-irem.univ-paris13.fr/site_spip/spip.php?article513

  
