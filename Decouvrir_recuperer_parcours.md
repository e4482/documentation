# Découvrir et récupérer des parcours de la Éléathèque

Découvrir un parcours

<https://tube-numerique-educatif.apps.education.fr/w/9mbFXrQJy4hKfFU6P374ii>

Récupérer un parcours

<https://tube-numerique-educatif.apps.education.fr/w/cojd6rhcmXmUDensZwgAy3>

## Étape 1 : Accéder à la Éléathèque

Pour accéder à la Éléathèque, il est nécessaire de se connecter à la plateforme et de s'identifier.

![page d'accueil élea](images/eleatheque/page_accueil.png)    ![page d'accueil élea](images/eleatheque/connexion.png)

Sur votre tableau de bord, cliquez sur "accéder à la Éléathèque".

![tableau de bord](images/eleatheque/acceder_eleatheque.png)


## Étape 2 : Affiner sa recherche par mots-clés

Pour affiner votre recherche par niveau, discipline, approche pédagogique ou thématique.
Vous pouvez :
* sélectionner un des mots-clés les plus fréquemment utilisés dans la liste à droite de l'écran :
![mots-clés](images/eleatheque/parcourir_eleatheque.png)
* taper votre recherche dans le champ texte (des propositions s'affichent en-dessous selon les caractères tapés) :
![recherche libre](images/eleatheque/parcourir_eleatheque2.png)

Puis cliquez sur "Valider"

Une sélection de parcours correspond à votre recherche s'affiche comprenant le titre du parcours et l'ensemble des mots-clés associés.

![sélection de parcours](images/eleatheque/selection_parcours.png)

## Étape 3 : Découvrir le contenu d'un parcours

Cliquez sur le titre du parcours pour obtenir des informations complémentaires :
* sur la partie droite s'affiche une description du parcours et le nom des auteurs.
* sur la partie gauche s'affiche le scénario du parcours avec l'ensemble des activités.

![exemple de présentation de parcours](images/eleatheque/exemple_parcours.png)

En cliquant sur la première activité du parcours ou sur "tester le parcours", vous accédez à la plateforme communaute.elea où sont stockés les parcours exemples académiques.
Sur cette plateforme, vous devez vous identifier avec votre compte académique.
Puis, vous pouvez consulter directement le parcours que vous avez choisi avec un statut d'enseignant non-éditeur.
Vous avez accès à toutes les parties y compris celles cachées aux élèves et vous n'êtes pas bloqué dans votre progression. Par contre, vous ne pouvez pas modifier le parcours.

![tester le parcours](images/eleatheque/tester_parcours.png)

Pour tester le parcours comme les élèves, cliquez sur le menu bleu puis sur "Prendre le rôle de..." et enfin choisissez "ÉTUDIANT" 

En cliquant sur une activité, vous accédez à la page correspondante sur le parcours sans avoir à réaliser la progression demandée aux élèves.


## Étape 4 : Télécharger un parcours

Si vous souhaitez récupérer un parcours pour le proposer à vos élèves tel quel ou le personnaliser, vous pourrez le télécharger depuis la Éléathèque.
Cependant, la fermeture prochaine de la plateforme « ScolawebTV » au profit du nouveau service d’hébergement vidéo « Peertube » implique pour les utilisateurs d’éléa de migrer toutes ces anciennes vidéos sur le nouveau service et de rééditer les codes d’intégration dans leurs parcours.
Tous les parcours de la Éléathèque contenant des vidéos de la ScolawebTV ont été actualisés et intègrent désormais des vidéos hébergées sur Peertube. Si vous testez un parcours, vous aurez bien accès au parcours mis à jour.
En revanche, l’archive que vous récupérez en cliquant sur le bouton « Télécharger le parcours » est obsolète et les vidéos qu’elles contiennent sont encore hébergées sur l’ancienne plateforme (il est à noter que si le parcours ne contient aucune vidéo, vous pourrez le récupérer en utilisant ce bouton et passer à la dernière étape).

Pour récupérer le parcours actualisé, veuillez suivre la procédure suivante :

Cliquez sur « Tester le parcours ».

![télécharger un parcours](images/eleatheque/accueil.png)

Allez dans le menu « Administration du cours ».

![menu1](images/eleatheque/menu1.png)

Cliquez sur le bouton « Sauvegarde ».

![menu2](images/eleatheque/menu2.png)

En bas de la page, cliquez sur « Passer à la dernière étape ».

![sauvegarde1](images/eleatheque/sauvegarde1.png)

Cliquez sur « continuer ».

![sauvegarde2](images/eleatheque/sauvegarde2.png)

Un message d’erreur apparaît. C’est tout à fait normal étant donné que vous ne possédez pas les droits pour restaurer ce parcours sur la Éléathèque.

![sauvegarde3](images/eleatheque/sauvegarde3.png)

En revanche, la sauvegarde du parcours est bien enregistrée dans votre compte académique.
Pour récupérer l’archive, cliquer sur « continuer », puis revenez sur votre page d’accueil à partir du fil d’ariane.

![lien-accueil](images/eleatheque/lien-accueil.png)

Vous pouvez maintenant restaurer le parcours sur la plateforme communauté, où récupérer l’archive afin de la restaurer sur votre plateforme de bassin.
Créez un nouveau parcours vide et donnez lui un nom.

![parcours-vide](images/eleatheque/parcours_vide.png)

Puis dans le menu « administration du cours », cliquez sur « restauration ».

![restauration](images/eleatheque/restauration.png)

Vous accédez au gestionnaire des sauvegardes. 

![restauration2](images/eleatheque/restauration2.png)

Vous pouvez désormais, 
* soit restaurer le parcours sur la plateforme communauté pour travailler avec des collègues de l’académie en cliquant sur « restaurer » (non utilisable avec les élèves)

* soit récupérer l’archive du parcours (fichier .mbz) pour le restaurer sur votre plateforme de bassin afin de le mettre à disposition de vos élèves.
Une fois le parcours téléchargé, suivez la procédure de [migration d'un parcours](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=faire-migrer-un-parcours "tutoriel pour migrer un parcours") à partir de l'étape de 2. Sur ce nouveau parcours, identique au parcours exemple académique, vous aurez des droits d'enseignant éditeur et aurez la possibilité de le modifier.





