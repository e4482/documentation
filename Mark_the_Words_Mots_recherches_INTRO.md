# Mark the Words (Mots recherchés)



Ce contenu interactif (H5P) permet de créer un texte dans lequel l'élève devra repérer certains mots respectant une consigne donnée. 

![Exemple d'énoncé d'une activité "Mark the Words" (Mots recherchés)](images/markthewordsmotscliques/markthewordsmotscliques01exempleenonce.png)

**Exemple** : dans cette phrase cliquez sur un adverbe :  "Longtemps, je me suis couché de bonne heure".

Une réponse incorrecte.

![Exemple d'une réponse incorrecte à une activité "Mark the Words" (Mots recherchés)](images/markthewordsmotscliques/markthewordsmotscliques02exemplefaux.png)

Une réponse correcte.

![Exemple d'une réponse correcte à une activité "Mark the Words" (Mots à cliquer)](images/markthewordsmotscliques/markthewordsmotscliques03exemplejuste.png)

Nous commencerons par ajouter un contenu interactif du type (en anglais) "**Mark the Words**".

![Icone de l'activité Mark the Words Mots cliqués](images/markthewordsmotscliques/markthewordsmotscliquesicone.png)