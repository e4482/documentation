# Créer un connecteur sur MonLycée.net

## Accéder à la console

Connectez-vous à l'ENT avec un compte administrateur. Si vous l'êtes bien, vous trouverez l'icône __Administration__ dans vos applications pour lancer la console :

![admin-icon](images/entidf/admin-icon.png)

Une fois dans celle-ci, sélectionnez __Applications__ dans le menu déroulant :

![admin-console-apps](images/entidf/admin-console-apps.png)

*Autre méthode, cliquez sur le lien suivant : <https://ent.iledefrance.fr/appregistry/admin-console> !*

## Créer un connecteur

Si votre établissement a demandé la reprise des données de l'ancien ENT, votre connecteur passé doit se trouver dans la liste.

### Avec reprise des données

Cliquez sur le connecteur Éléa pour afficher ses paramètres actuels :

![admin-console-apps](images/entidf/admin-cas-params-lilie.png)

### Sans reprise des données

1. Cliquez sur __Connecteurs & liens__
2. Sélectionnez votre établissement dans la colonne de gauche
3. Ajoutez un nouveau connecteur grâce au __+__

![admin-apps-addnewcas](images/entidf/admin-apps-addnewcas.png)

## Paramétrer votre connecteur

### Avec reprise des données

1. Complétez le champ __Icône__ comme indiqué ci-après
2. Enregistrez

__ATTENTION__ : Ne modifiez pas le champ __Identifiant__ !

### Sans reprise des données

Il faut tout d'abord...

![admin-cas-showparams](images/entidf/admin-cas-showparams.png)

Utilisez le tableau et la capture d'écran ci-dessous pour compléter les champs :

| Paramètre       | Valeur                                                              |
|-----------------|---------------------------------------------------------------------|
| Identifiant     | __elea__ suivi de votre __UAI/RNE__ (sans espace, sans majuscule)   |
| Nom d'affichage | Éléa                                                                |
| Icône           | https://evry.elea.ac-versailles.fr/theme/vital/pix/logo.png         |
| URL             | https://evry.elea.ac-versailles.fr/login/index.php?authCAS=LILIE    |

__ATTENTION__ : Ces deux liens sont valables uniquement pour les établissements du bassin d'Évry. Vous devez les adapter à votre situation en vous aidant du tableau en annexe !

![admin-cas-params](images/entidf/admin-cas-params.png)

__ATTENTION__ :

* Il s'agit des paramètres d'un établissement bien particulier, l'identifiant lui est personnel !
* Vous cocherez la case __Champs spécifiques CAS__ comme sur la capture d'écran ci-dessus.
* N'oubliez surtout pas de cliquer sur __Créer__.

## Activer les droits

Pour terminer, décidez qui doit pouvoir accéder à ce connecteur :

![admin-apps-accessrights](images/entidf/admin-apps-accessrights.png)

Vous devriez donner l'accès aux profils suivants :

* Tous les enseignants
* Tous les élèves
* Tous les personnels

*Les parents n'ont pas accès à Éléa puisqu'ils n'ont pas vocation à créer des parcours et qu'il n'est pas attendu qu'ils réalisent les exercices ;-)*

## Annexe - adresse url de chaque bassin

| Nom          | Adresse                                       |
|--------------|-----------------------------------------------|
|Antony        | https://antony.elea.ac-versailles.fr          |
|Argenteuil    | https://argenteuil.elea.ac-versailles.fr      |
|Boulogne      | https://boulogne.elea.ac-versailles.fr        |
|Cergy         | https://cergy.elea.ac-versailles.fr           |
|Enghien       | https://enghien.elea.ac-versailles.fr         |
|Etampes       | https://etampes.elea.ac-versailles.fr         |
|Évry          | https://evry.elea.ac-versailles.fr            |
|Gennevilliers | https://gennevilliers.elea.ac-versailles.fr   |
|Gonesse       | https://gonesse.elea.ac-versailles.fr         |
|Mantes        | https://mantes.elea.ac-versailles.fr          |
|Massy         | https://massy.elea.ac-versailles.fr           |
|Montgeron     | https://montgeron.elea.ac-versailles.fr       |
|Mureaux       | https://mureaux.elea.ac-versailles.fr         |
|Nanterre      | https://nanterre.elea.ac-versailles.fr        |
|Neuilly       | https://neuilly.elea.ac-versailles.fr         |
|Poissy        | https://poissy.elea.ac-versailles.fr          |
|Pontoise      | https://pontoise.elea.ac-versailles.fr        |
|Rambouillet   | https://rambouillet.elea.ac-versailles.fr     |
|Sarcelles     | https://sarcelles.elea.ac-versailles.fr       |
|Savigny       | https://savigny.elea.ac-versailles.fr         |
|Saint-Germain | https://sgl.elea.ac-versailles.fr             |
|Saint-Quentin | https://sqy.elea.ac-versailles.fr             |
|Vanves        | https://vanves.elea.ac-versailles.fr          |
|Versailles    | https://versailles.elea.ac-versailles.fr      |
