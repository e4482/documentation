# Vidéo issue de notre service historique (l'intégration fonctionne)

<https://scolawebtv.crdp-versailles.fr/?id=65784>

# Vidéo issue du premier nouveau service PodEduc (l'intégration ne fonctionne pas pour l'instant)

<https://podeduc.apps.education.fr/video/1525-capsule-bbb-volet-2-pix/>

REGEX : on acceptera aussi l'absence du dernier slash dans l'URL :

<https://podeduc.apps.education.fr/video/1525-capsule-bbb-volet-2-pix>

# Vidéo issue du second nouveau service PeerTube (l'intégration ne fonctionne pas pour l'instant)

<https://tube-numerique-educatif.apps.education.fr/w/vzxwF3yzJn7HAZSdfiwS2D>
