# Image Hotspots (Image réactive)

<https://tube-numerique-educatif.apps.education.fr/w/pAu4CZrWYpv2DbsJoYjuzf>

Ce contenu interactif permet de créer des images ou des infographies réactives : des "puces" sont réparties sur une image d'arrière plan ; un clic sur une de ces puces donne accès à des informations sous forme de textes, d'images ou de vidéos.

![Exemple d'image ou d'infographie](images/hotspotsinfographiereactive/hotspotsinfographiereactive01exemple.png)



Il est ainsi possible d'enrichir n'importe quel document préexistant (enregistré sous forme d'une image) avec des points d'intérêt cliquables qui fournissent des informations sur certains détails représentés.

![Exemple de puce cliquée](images/hotspotsinfographiereactive/hotspotsinfographiereactive02exemple.png)

Nous commencerons par créer un contenu interactif de type (en anglais) "**Image Hotspots**".

![Icone Hotspots Image réactive](images/hotspotsinfographiereactive/hotspotsinfographiereactiveicone.png)
