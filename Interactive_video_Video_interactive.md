## Intégrer une vidéo de Portail Tubes d'Apps-éducation

Connectez-vous au [portail Tubes](https://tubes.apps.education.fr/#) et sélectionnez la vidéo que vous voulez rendre interactive.

Sous la vidéo, cliquez sur les "**...**", puis cliquez sur **Télécharger**.

![Bouton H5P](images/interactive_video/Capture1.png)

 Choisissez la qualité vidéo qui vous convient (720p est un bon compromis), puis copiez le lien de la vidéo en cliquant sur le bouton **COPY**.

![Bouton H5P](images/interactive_video/Capture2.png)

 Le lien sera ensuite à coller dans le champ d'import de la vidéo (étape 1) dans votre activité H5P.

![Bouton H5P](images/interactive_video/Capture3.png)

## Intégrer une vidéo de PodEduc d'Apps-éducation

Connectez-vous au [portail PodEduc](https://podeduc.apps.education.fr/) et sélectionnez dans votre espace une vidéo que vous avez déposée.

Sous votre vidéo, vous trouvez un bouton de partage de la vidéo :

![Bouton H5P](images/interactive_video/Capture4.png)

En cliquant sur ce lien, une page apparaît dans laquelle vous allez pouvoir récupérer un lien permettant d'intégrer votre vidéo dans une activité H5P. Copiez le lien correspondant à la définition qui vous convient.

![Bouton H5P](images/interactive_video/Capture5.png)

 Le lien sera ensuite à coller dans le champ d'import de la vidéo (étape 1) dans votre activité H5P.

![Bouton H5P](images/interactive_video/Capture3.png)

