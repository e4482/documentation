# Procédure de réinitialisation du mot de passe

Si vous avez oublié votre mot de passe, une procédure de réinitialisation est prévue lors de la phase de connexion à éléa :

## Étape 1
1. Rendez-vous sur la page d'accueil de votre plateforme et cliquez sur **"Démarrer"**

![page_accueil_elea](images/accueil_elea/capture1.png)

2. Cliquez sur **"Utiliser mon compte local"**

Si vous n'utilisez pas de compte local, cette procédure ne vous concerne pas.

![page_accueil_elea](images/accueil_elea/capture4.png)

3. Cliquez sur **"Vous avez oublié votre nom d'utilisateur et/ou votre mot de passe"**

![page_accueil_elea](images/accueil_elea/capture5.png)



## Étape 2

Renseignez votre nom d'utilisateur, puis cliquez sur **"RECHERCHER"**

![recuperation](images/accueil_elea/mdp_oubli.png)

