# Découvrir les gabarits

<https://tube-numerique-educatif.apps.education.fr/w/cgKP6vCMwaPNPuKwaBDEGb>

# Découvrir les éléments qui composent un parcours

<https://tube-numerique-educatif.apps.education.fr/w/oa6y1pMw2ogWCoxXXxwXds>


# Personnaliser les sections

<https://tube-numerique-educatif.apps.education.fr/w/hGzeZPWufAKqp5kaD6QuKF>

# Cacher les sections sur la page d'accueil

<https://tube-numerique-educatif.apps.education.fr/w/w1eHKi3XxUba49bcKuoeTZ>

# Suivre la progression des élèves depuis la page d'accueil

<https://tube-numerique-educatif.apps.education.fr/w/aZv1516EmC8nyBuWUFxK7T>

# Suivre la progression des élèves depuis le carnet de notes

<https://tube-numerique-educatif.apps.education.fr/w/4K7pQFUqqpYQkmDm46V2St>

# Suivre la progression des élèves depuis les activités

<https://tube-numerique-educatif.apps.education.fr/w/aSZeyUQHhkuLREqRwPL1oz>


# Paramétrer le suivi d'activité des élèves

<https://tube-numerique-educatif.apps.education.fr/w/1pQUX8acT2EEm8pLUJdRme>

# Déplacer une activité

<https://tube-numerique-educatif.apps.education.fr/w/xzdzX6aij5sRAv9FFhk7ux>

# Inscrire des collègues

<https://tube-numerique-educatif.apps.education.fr/w/2gUrtEa2fbo2XsqFxfr2Sc>

# Ajouter un badge

<https://tube-numerique-educatif.apps.education.fr/w/822fCiijaei2Qcxuttmpmy>

# Consulter et modifier son profil utilisateur

<https://tube-numerique-educatif.apps.education.fr/w/p8YB8e4YdqUQgcF61uwbmF>

# Insérer un lien ou un fichier derrière une image dans un parcours 

<https://tube-numerique-educatif.apps.education.fr/w/dHS5kjfxn4cLnRrGN6ysPA>

# Intégrer des descriptions pour les images d'un parcours

<https://tube-numerique-educatif.apps.education.fr/w/exYPkgGzj1sHUdxJddNKkC>

# Intégrer rapidement des consignes audio ou vidéo

<https://tube-numerique-educatif.apps.education.fr/w/sScY7fRuA7vge2kHRjv15o>

# Commencer à scénariser avec un Parcours Express

<https://tube-numerique-educatif.apps.education.fr/w/w3erh81hn86jYJNeNUz7RD>

# Mobiliser différentes modalités d'apprentissage dans un parcours

<https://tube-numerique-educatif.apps.education.fr/w/8AYgUCpGzav8rDazqTcgsN>

# Accompagner et guider les élèves dans un parcours

<https://tube-numerique-educatif.apps.education.fr/w/vfY7JnHjCwdvaAuVUeW6iG>

# Réinitialiser un parcours

<https://tube-numerique-educatif.apps.education.fr/w/ayAXLVxZfcJYfYNXEPbjtb>

# Afficher un parcours en mode élève/étudiant

<https://tube-numerique-educatif.apps.education.fr/w/dTwqjc3jp5E8Qgc6sgCFMP>
