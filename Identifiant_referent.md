Si vous êtes référent numérique, le nom d'utilisateur du compte de gestion de l'établissement correspond à l'**UAI/RNE** de ce dernier.

Un mail contenant un lien permettant la réinitialisation du mot de passe sera envoyé sur l'adresse mail de l'établissement (ce.UAI@ac-versailles.fr)

**Attention** : Ce lien n'est valable que pendant **30 minutes**.

![mail_lien_réinitialisation](images/accueil_elea/mail_lien_reinitialisation.png)

1. Cliquez sur le lien. 

2. Une nouvelle page s'ouvre, cliquez sur **"CONTINUER"** 

![continuer](images/accueil_elea/mdp_oubli2.png)

3. Définissez votre nouveau mot de passe puis cliquez sur **"ENREGISTRER"**

![réinitialisation du mot de passse](images/accueil_elea/mdp_oubli3.png)

Vous pouvez de nouveau vous connecter.