# Comment accéder à Éléa tout en limitant la charge sur les serveurs des ENT ?

En cette période exceptionnelle, les ENT subissent une charge très élevée sur leurs serveurs. Tous travaillent accuellement à décongestionner leurs infrastructures mais vous pouvez leur faciliter la tâche en adaptant vos habitudes. Voici comment faire !

## Donnez rendez-vous à vos élèves directement sur Éléa

Vos élèves et vous-même avez sans doute l'habitude de rejoindre Éléa en cliquant sur une icône dans votre ENT. Cette méthode requiert que vous puissiez vous connecter puis naviguer dans l'ENT alors que ce n'est pas nécessaire.

Ci-après, la procédure que vous et vos élèves devriez suivre pour minimiser la charge subie par votre ENT.

> **Attention : ** Malheureusement cette procédure ne fonctionne pas pour l'ENT OZE et peut être impactée par les restrictions d'accès imposées aux élèves.

### 1. Connectez-vous à la plateforme Éléa de votre bassin

L'académie est divisée en 24 bassins d'éducation et chacun dispose d'une plateforme Éléa qui lui est propre. Rendez-vous donc directement à l'adresse URL de votre plateforme :

| Nom          | Adresse                                       |
|--------------|-----------------------------------------------|
|Antony        | https://antony.elea.ac-versailles.fr          |
|Argenteuil    | https://argenteuil.elea.ac-versailles.fr      |
|Boulogne      | https://boulogne.elea.ac-versailles.fr        |
|Cergy         | https://cergy.elea.ac-versailles.fr           |
|Enghien       | https://enghien.elea.ac-versailles.fr         |
|Etampes       | https://etampes.elea.ac-versailles.fr         |
|Évry          | https://evry.elea.ac-versailles.fr            |
|Gennevilliers | https://gennevilliers.elea.ac-versailles.fr   |
|Gonesse       | https://gonesse.elea.ac-versailles.fr         |
|Mantes        | https://mantes.elea.ac-versailles.fr          |
|Massy         | https://massy.elea.ac-versailles.fr           |
|Montgeron     | https://montgeron.elea.ac-versailles.fr       |
|Mureaux       | https://mureaux.elea.ac-versailles.fr         |
|Nanterre      | https://nanterre.elea.ac-versailles.fr        |
|Neuilly       | https://neuilly.elea.ac-versailles.fr         |
|Poissy        | https://poissy.elea.ac-versailles.fr          |
|Pontoise      | https://pontoise.elea.ac-versailles.fr        |
|Rambouillet   | https://rambouillet.elea.ac-versailles.fr     |
|Sarcelles     | https://sarcelles.elea.ac-versailles.fr       |
|Savigny       | https://savigny.elea.ac-versailles.fr         |
|Saint-Germain | https://sgl.elea.ac-versailles.fr             |
|Saint-Quentin | https://sqy.elea.ac-versailles.fr             |
|Vanves        | https://vanves.elea.ac-versailles.fr          |
|Versailles    | https://versailles.elea.ac-versailles.fr      |

Une fois sur la page d'accueil, cliquez sur **Démarrer**.

![accueil](images/limiter_charge_ent/accueil.png)

### 2. Choisissez le bouton orange correspondant à votre ENT

L'image ci-dessus est une capture de la plateforme du bassin d'Étampes, nous n'y voyons donc que les ENT disponibles dans l'Essonne (le connecteur des collèges et celui des lycées).

![connecteurs](images/limiter_charge_ent/connecteurs.png)

> **Remarque : **
>
> Le connecteur académique et le bouton d'accès aux comptes locaux sont toujours présents.
>
> Ce connecteur académique est reservé **aux seuls enseignants** et permet d'accéder à **un second compte qui vous est personnel**. Si vous l'utilisez, vous ne passerez plus du tout par votre ENT.
>
> Comme il s'agit d'un compte différent, apprenez à disposer de l'ensemble de vos parcours sur vos deux comptes : [Accéder au tutoriel](https://communaute.elea.ac-versailles.fr/local/faq/?role=hidden&element=se-connecter-a-elea-en-cas-de-fermeture-de-l-ent)

### 3. Identifiez-vous comme si vous vous rendiez sur votre ENT

C'est bien votre ENT qui vous identifie sur Eléa mais via un serveur dédié à l'authentication. De cette manière, la charge supportée par votre ENT est minimale.

Vous devriez pouvoir atteindre la tableau de bord d'Éléa même si votre ENT rencontre des difficultés !

![tableaudebord](images/limiter_charge_ent/tableaudebord.png)



## Faîtes parvenir des liens profonds à vos élèves

Vous pouvez aller encore plus loin en fournissant directement à vos élèves des liens dits "profonds" vers vos parcours ou même vers une activité au sein de l'un d'eux.

### 1. Avez-vous moyen de diffuser de tels liens sans ENT ?

Assurez-vous avant toute chose que vous pourrez fournir ces liens à vos élèves autrement qu'en passant par l'ENT puisque le but est toujours de diminuer la charge sur les ENT.

Plusieurs solutions s'offrent à vous. Voici quelques idées où déposer ces liens :

* sur le site Internet de l'établissement ou de l'école,
* sur un blog public de classe (http://blog.ac-versailles.fr/),
* dans le cahier de texte des élèves (s'il ne s'agit pas d'un service founi par l'ENT),
* en direct depuis une classe virtuelle CNED (http://www.dane.ac-versailles.fr/continuite-cned/cned-accompagnement-ma-classe-a-la-maison-classes-virtuelles)
* sur un service de microblogging public,
* etc.

> **Remarque : **Certains ENT permettent de créer une page publique. L'ENT reste alors indispensable mais les utilisateurs n'ont plus à se connecter.

### 2. Où trouver le lien à transmettre aux élèves ?

Deux solutions s'offrent à vous.

#### À partir du Teacherboard

Cliquez droit sur le nom de votre parcours puis copier l'adresse du lien depuis le menu contextuel.

![urlclicdroit](images/limiter_charge_ent/urlclicdroit.png)

#### En vous rendant directement à la page concernée

1. Allez dans le cours ou l'activité que vous souhaitez partager ;
2. Copiez l'URL s'affichant dans la barre d'adresse de votre navigateur.

> **Exemples : **
>
> * Pour un cours : 
>
>   ![urlparcours](images/limiter_charge_ent/urlparcours.png)
>
> * Pour une activité : 
>
>   ![urlactivite](images/limiter_charge_ent/urlactivite.png)