# Intégrer une ressource Étiquette

<https://tube-numerique-educatif.apps.education.fr/w/wZutwt3qyuU7uN4BWebtMs>

# Intégrer une ressource Étiquette invisible pour les élèves

<https://tube-numerique-educatif.apps.education.fr/w/5NYK75a4YAj1Uo4FReb2ve>

# Intégrer une ressource URL

<https://tube-numerique-educatif.apps.education.fr/w/wQgADp9CC8pmR1BJuHNB5p>

# Intégrer une ressource Dossier

<https://tube-numerique-educatif.apps.education.fr/w/4qzdpNBGhsGAWBufvF4rZD>
