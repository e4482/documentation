# Fill in the Blanks (Texte à "trous")



Ce contenu interactif (H5P) permet de créer des textes à "trous" : ce sera à l'élève de compléter correctement ces "blancs" du texte en fonction du contexte.

![Un exemple d'énoncé "Fill in the Blanks"](images/fillintheblankstexteatrous/fillintheblankstexteatrous01exemple.png)

Une réponse incorrecte.

![Un exemple de réponse fausse](images/fillintheblankstexteatrous/fillintheblankstexteatrous02exemplereponsefausse.png)

La réponse correcte.

![Un exemple de réponse juste](images/fillintheblankstexteatrous/fillintheblankstexteatrous03exemplereponsejuste.png)

Nous commencerons par créer un contenu interactif de type (en anglais) "**Fill in the Blanks**".

![Icone de l'activité Fill in the Blanks Texte à Trous](images/fillintheblankstexteatrous/fillintheblankstexteatrousicone.png)