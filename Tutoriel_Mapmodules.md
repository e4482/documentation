# Intégrer une activité Carte de progression

<https://tube-numerique-educatif.apps.education.fr/w/gs6CZMi88fCqZkerhHjfAk>

L'activité "**Carte de progression**" permet de créer une image dont l’apparence variera pour chaque apprenant au fil de son avancement dans le parcours.

Cette carte de progression combine :

* une image sélectionnée par l’enseignant en « toile de fond »
* et des icônes.

Éléa affichera automatiquement au-dessus de l’image choisie une icône pour chaque activité présente dans le parcours.

L’apparence de cette icône variera au gré de la progression de l’apprenant dans le parcours.

L’icône d’une même activité aura ainsi trois apparences possibles :

​    • L’icône peut être par exemple un cadenas fermé tant que l’apprenant n’a pas encore accès à cette activité ;

![Capture d’écran de 2019-10-10 16-20-16](images/mapmodules/icon_close.png)

​    • Cette icône se transformera en un point d'interrogation dès que l’activité deviendra accessible à l’apprenant ;

![Capture d’écran de 2019-10-10 16-19-32](images/mapmodules/icon_available.png)

​    • Enfin, cette icône se présentera sous forme d’une coche une fois l’activité terminée par l’apprenant.

![Capture d’écran de 2019-10-10 16-19-46](images/mapmodules/icon_done.png)

La carte de progression permet donc à l’enseignant de donner une identité visuelle et thématique à son parcours.

C’est également un élément engageant de gamification du parcours : l’apprenant peut à tout moment apprécier visuellement le chemin qu’il a déjà parcouru dans le scénario du parcours et anticiper les activités qui lui restent à consulter.

## Paramétrage du Parcours

Pour commencer, il est nécessaire de vérifier que le suivi d’achèvement du parcours est activé car c’est lui qui conditionne - sur la carte de progression - l’apparence de l’icône (fermée/accessible/terminée) d’une activité donnée.

Dans le menu latéral suivre :

1. Administration du cours
2. Paramètres
3. Suivi d’Achèvement

« **Activer le suivi d’achèvement des activités **» doit être en position « Oui ».

![Capture d’écran de 2019-10-10 16-27-28](images/mapmodules/suivi.png)


Pour utiliser au mieux l’activité « **Carte de progression **», il est nécessaire de se familiariser avec ces deux paramétrages des activités  :

* « Achèvement d’activité » qui fixe les conditions sous lesquelles une activité est considérée comme achevée ou non par l’apprenant (achevée par le simple affichage de l’activité par l’apprenant, ou bien par l’obtention d’une note) ;
  
* Et « Restriction d’accès » : paramétrage qui définit sous quelles conditions est accessible et/ou affichée l’activité ; ces conditions sont définies en fonction de l’achèvement ou non d’une ou plusieurs autres activités du parcours.

(ces paramétrages sont disponibles dans les paramétrages de toutes les activités d’Éléa : Test, Leçon, Étiquette, etc.)

## Création de l’activité carte de progression

Pour créer une activité "Carte de Progression" :

1. Ouvrir le parcours pour lequel la carte suivra la progression des élèves ;
2. Activer le mode édition ;
3. Sélectionner la section dans laquelle apparaîtra la carte ;
4. Cliquer sur "Ajouter une activité ou une ressource" ;
5. Basculer sur l’onglet "Mode Avancé" ;
6. Sélectionner l'activité "Carte de progression" dans la liste ;
7. et enfin cliquer sur "Ajouter". 

![Capture d’écran de 2019-10-10 18-31-30](images/mapmodules/bouton_carte_progression.png)

Une nouvelle page s'ouvre pour définir les paramètres de la carte.

## Choix de la toile de fond de la carte de progression

Sur cette page de paramétrage, sous « **Choisir une image **», trois options permettent de sélectionner l’image qui servira de toile de fond à la carte de progression du parcours.

![Capture d’écran de 2019-10-10 16-23-36](images/mapmodules/selection_image.png)

1. Cliquer sur « Choisir un Fichier » permet de parcourir les fichiers du parcours pour y récupérer une image préexistante ou y déposer une nouvelle image (« Déposer un fichier ») ;

2. « Utiliser une carte standard » offre de choisir une image parmi un jeu de 32 illustrations déjà disponibles dans Éléa.

![Capture d’écran de 2019-10-10 16-26-10](images/mapmodules/cartes_biblio.png)

>**Remarques :**
>
>* un set d’icônes est présélectionné pour chacune de ces illustrations ainsi qu’un « chemin » prédéfini pour disposer ces icônes sur la carte - voir infra **« Tracé du Chemin »**.
>
>Si la case **« Charger le chemin et les icônes correspondants » **est cochée tout choix préalable d’icônes et tout tracé de chemin entamé seront supprimés.
>
>* Le type KHAN : si la case "**Utiliser le mode de carte spécifique Khan**" est cochée il n'y aura pas d'image dans ce type de **Carte de progression** , les activités apparaîtront de manière linéaire en colonne.*
>
>![Capture d’écran de 2019-10-10 16-24-07](images/mapmodules/khan.png)

3. Enfin, il est possible de glisser-déposer l’image de votre choix depuis un dossier de votre ordinateur directement dans la zone en pointillés libellée « Vous pouvez glisser des fichiers ici pour les ajouter ».

Dans les trois cas Éléa affiche l’image souhaitée. 

> **Conseil :**
>
> On veillera à choisir des images dont les dimensions et la résolution assurent une lecture facile. Une image trop riche en détails auxquels viendront se superposer les icônes des activités peut être rapidement illisible.

## Choix des Icônes

 En bas à gauche de la carte choisie apparaît un set des trois apparences possibles d’une même icône (selon que l’activité correspondante sera "fermée"," accessible" ou "terminée"). 

Ce set n’est qu’indicatif (exemple d’apparences possibles ; ces icônes n’apparaîtront pas à cette même place sur la carte définitive).

Pour sélectionner le set type des icônes qui apparaîtront sur la carte, dérouler le menu **« Jeu d’icônes des activités »** et cliquer sur le nom du set souhaité (l'affichage des trois icônes d’exemple s’actualise immédiatement en bas à gauche de la carte).

Cliquer dans le menu déroulant **« Taille des icônes »** pour fixer la dimension souhaitée des icônes de la carte.

![Capture d’écran de 2019-10-10 18-30-28](images/mapmodules/jeux_icones.png)

## Tracé du Chemin

Éléa va placer automatiquement sur la carte une icône pour chaque activité du parcours.

Ces icônes seront régulièrement espacées entre le point de départ et le point d’arrivée d’un chemin à la manière de perles sur un fil. Leur ordre correspondra à l'ordre des activités correspondantes dans les sections du parcours.

![Capture d’écran de 2019-10-10 16-23-20 CRAYON](images/mapmodules/crayon.png)

Pour tracer ce chemin :

1. Cliquer sur l’icône crayon sous la carte (la bordure vire au gris) ;
2. Choisir le type de tracé voulu. Il existe deux modalités de tracé : le tracé anguleux (icônes sur ligne brisée) ou le tracé courbe (icône sur courbe) ;

​																	![Capture d’écran de 2019-10-10 16-23-20 ANGLES](images/mapmodules/ligne_brisee.png) ou ![Capture d’écran de 2019-10-10 16-23-20 ANGLES](images/mapmodules/courbe.png)

3. Cliquer-relâcher sur la carte à l’endroit choisi pour le point de départ du chemin ;
4. Cliquer-relâcher pour placer le deuxième point, et ainsi de suite pour chacun des points suivants. Le chemin est automatiquement tracé entre deux points consécutifs. Il s’agit de placer les points là où le chemin que l’on trace marque des changements de directions appuyés ;
5. Pour terminer le tracé, double-cliquer pour marquer le point d’arrivée, ou cliquer n'importe où sur la page en dehors de la carte, ou bien encore faire un clic droit.

![Capture d’écran de 2019-10-10 16-25-26](images/mapmodules/carte_ligne_brisee.png)

Ce chemin ne sera pas affiché dans l’affichage final de la **Carte de progression**: il est le fil invisible sur lequel seront distribuées régulièrement les icônes des activités.

>**Note :**
>
>On pourra dans certains cas faire le choix de faire épouser au chemin tracé des courbes et lignes d’un élément notable déjà présent dans l'image de la carte sous-jacente ; les icônes des activités viendront ainsi se superposer le long de cet élément.

## Modifier le tracé du chemin

Une fois le chemin tracé celui-ci peut être modifié.

Pour déplacer un point du chemin : cliquer et maintenir le clic pour glisser et redéposer le point à son nouvel emplacement sur la carte.

Si la modalité choisie est le tracé **« Courbe »** il est aussi possible de modifier l’inflexion de chaque courbe grâce aux deux poignées bleues situées de part et d’autre de chaque point du chemin.

![Capture d’écran de 2019-10-10 16-25-54](images/mapmodules/carte_courbe.png)

Cliquer sur l’icône gomme pour effacer entièrement le chemin.

![Capture d’écran de 2019-10-10 16-23-20 GOMME](images/mapmodules/gomme.png)

## Éléments supplémentaires de description à afficher

Il est possible ici de rédiger un titre ou une légende au-dessus et/ou en-dessous de la carte de progression.

![Capture d’écran de 2019-10-10 16-24-22](images/mapmodules/elements_description.png)

Ne pas oublier d'**enregistrer** la carte.

Toute activité ajoutée par la suite au parcours viendra s'insérer automatiquement sur cette carte de progression.
