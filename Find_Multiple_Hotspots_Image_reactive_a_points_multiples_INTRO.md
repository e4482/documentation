# Find Multiple Hotspots (Image réactive à points multiples)

Ce contenu interactif se présente sous la forme d'une simple image d'arrière-plan : l'élève est invité par la consigne à y observer et déterminer certains éléments "clefs" sur lesquels il faudra cliquer. 

![Exemple d'activité de Find Multiple Hotspsots Image réactive à points multiples](images/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer01exemplefindmultiplehotspotsimageacliquer.png)

Dans cet exemple l'élève doit cliquer sur les drapeaux des pays qui ne sont pas membres de l'U.E.

![Exemple d'activité de Find Multiple Hotspsots Image réactive à points multiples](images/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquer02exemplefindmultiplehotspotsimageacliquer.png)

La diversité des supports utilisables (schémas, photographies, illustrations variées) fait de ce contenu interactif une activité très polyvalente qui favorise de manière générale l'observation bien sûr, mais qui pourra par exemple amener l'élève à devoir effectuer des comparaisons entre divers éléments pour en déterminer des propriétés caractéristiques, à établir des contrastes ou à déterminer des points communs, ou bien encore à détecter des erreurs dans une image donnée.

La création d'une telle activité s'effectue en deux temps :

- On téléverse d'abord l'image d'arrière-plan.
- Puis, au-dessus de cette image, on trace des "zones réactives", c'est-à-dire des zones qui resteront invisibles de l'élève mais réagiront si on clique dessus en validant ou invalidant ce clic comme étant une bonne ou mauvaise réponse.

Nous commencerons par créer un contenu interactif de type (en anglais) "**Find Multiple Hotspots**".

![Icône de l'activité Find Multiple Hotspots Image réactive à points multiples](images/findmultiplehotspotsimageacliquer/findmultiplehotspotsimageacliquericone.png)