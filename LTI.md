Ouvrez parallèlement votre plateforme de bassin et la plateforme Test.

Sur la plateforme Test, sur le teacherboard, choisissez le parcours que vous souhaitez publier comme outil LTI et ouvrez-le.

Cliquez sur le menu bleu puis sur "**Administration du cours**" et "**Publiés comme outils LTI**" en bas du menu.

![administration du cours](images/lti/administration_du_cours.png)![publiés comme outils LTI](images/lti/publies_comme_outils_lti.png)

Une nouvelle page s'ouvre. Cliquez sur "**Ajouter**".

![ajouter](images/lti/ajouter.png)

Vous pouvez alors choisir de partager le cours complet ou bien une activité du parcours dans l'onglet "**Outils à publier**".

![choix cours ou activités](images/lti/choix_cours_activite.png)

Cliquez ensuite sur "**Ajouter une méthode**"

![ajouter méthode](images/lti/ajouter_methode.png)

Copiez les détails du lancement (URL de la cartouche et Secret).

![details du lancement](images/lti/details_lancement.png)

Sur votre plateforme de bassin, ouvrez le parcours sur lequel vous souhaitez ajouter l'activité publiée comme outil LTI (ou bien ouvrez un parcours vide). Activez le mode édition puis cliquez dans la section souhaitée sur "**Ajouter une activité ou une ressource**"

![ajouter une activite ou une ressource](images/lti/ajouter_activite_ressource.png)

Sélectionnez le mode avancé puis "**Outil externe**" et enfin cliquez sur "**Ajouter**"

![mode avancé](images/lti/mode_avance.png)

![outil externe](images/lti/outil_externe.png)

Dans la fenêtre qui s'ouvre, donnez un nom à l'activité puis collez dans le champ URL de l'outil l'URL du cartouche que vous avez copiée précédemment. Procédez de même pour le champ secret.

![ajout outil externe](images/lti/selection_contenu.png)

Cliquez sur "**Afficher plus**"

![afficher plus](images/lti/selectionner_un_contenu.png)

Choisissez une clé client (par exemple intégration 1). Si vous êtes amené à intégrer cette même activité (ou cours) dans un autre parcours, la clé client devra être différente (intégration 2).

Dans l'onglet "**Conteneur de lancement**", différentes options s'offrent à vous :

- Pour un cours entier, choisissez fenêtre existante
- Pour une activité, 2 types d'affichage sont possibles :
  - par défaut, les informations de la plateforme test seront intégrées dans votre plateforme de bassin.

![lancement intégré](images/lti/lancement_integre.png)

  - Mais il est possible d'afficher les informations dans une nouvelle fenêtre. Attention, les élèves devront quitter cette nouvelle fenêtre pour revenir sur le parcours initial.

![lancement dans une nouvelle fenêtre](images/lti/lancement_nouvelle_fenetre.png)

Si vous souhaitez partager une seconde activité, il faudra recommencer la procédure une nouvelle fois.

