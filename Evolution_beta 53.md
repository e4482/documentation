# Mise à jour Éléa - Bêta 53

Le service Éléa est proposé aux écoles et aux établissements de l'académie de Versailles 
par le biais de 25 plateformes : une pour chacun des 24 bassins d'éducation 
et une dernière permettant à tous les enseignants de collaborer.

Éléa repose sur le projet libre Moodle. Au fil des mises à jour, nous profitons ainsi 
des nouveautés de cet outil en plus des nouvelles fonctionnalités spécifiques que nous 
développons pour nos propres besoins.

**Au coeur de cette nouvelle mouture, un saut majeur de version Moodle !
Nous sommes passés de la branche 3.2 à la 3.5.**

**Découvrez ci-après les principales incidences...**

## Le Teacherboard (développement spécifique)

* De nouveaux gabarits repensés pour créer vos parcours

    L'assistant a été totalement revu pour vous aider dans cette tâche. 
    Une seconde étape permet de définir un nom personnalisé pour votre parcours 
    ainsi que d'en modifier le dossier de destination.
    
    Le parcours vide ne contient plus qu'une seule section sans titre par défaut et il n'est donc 
    plus nécessaire de créer suffisamment de sections pour restaurer un parcours dans sa totalité.

![nouveaux gabarits](images/capture-elea-B53/gabarits.png)

![création d'un parcours](images/capture-elea-B53/nouveau_parcours.png)

* Un peu d'aide pour faciliter l'inscription et la désinscription des élèves 
et des professeurs dans vos parcours

    Lorsque vous inscrivez une classe ou un groupe, la validation est dorénavant instantanée.
    
    Si vous perdez vos droits par erreur sur un parcours, 
    laissez passer une nuit et vous en récupérerez la pleine possession automatiquement.

![création d'un parcours](images/capture-elea-B53/inscription_classes.png)

![création d'un parcours](images/capture-elea-B53/inscription_profs.png)

* La possibilité de se désinscrire du parcours Louise

    En cas de besoin, vous pourrez le retrouver dans la Éléathèque.

## Module supplémentaire

Le module GeoGebra est maintenant disponible sur toutes nos plateformes. 
Pour l'utiliser, ajoutez une activité à l'un de vos parcours en choisissant le mode avancé.

Il n'y a pas de tutoriel pour le moment mais cela viendra rapidement.

## Correctifs (développement spécifique)

* Les cartes de progression affichent les activités après restauration d'un parcours 
sans action de votre part.

* Les images sont correctement sauvegardées - et donc restaurées - dans les activités de type QCM.

* Le chronomètre des activités Test qui l'utilisent s'affiche à l'écran plutôt que dans le menu.

## Le coeur Moodle

* Enregistrer directement du son ou une courte vidéo depuis l'éditeur 
sans Flash (technologie dépréciée)

    Les contenus Flash intégrés dans vos parcours ne sont pas impactés.

![bandeau_audio_video](images/capture-elea-B53/bandeau_avec_audio_video.png)

* Modifier l'achèvement des activités par lot et définir de nouvelles valeurs par défaut

    **Dans votre parcours > Menu bleu > Administration du cours > Achèvement du cours**

* Visualiser et modifier l'état d'achèvement de toutes les activités pour tous les élèves

![achèvement d'activités](images/capture-elea-B53/achevement_activites.png)

* Gérer plus simplement la personnalisation des droits dans vos parcours 
grâce à la recherche alphabétique et à la sélection multiple

![gestion des participants](images/capture-elea-B53/participants.png)

* Programmer un rappel pour penser à évaluer les élèves

![rappel évaluation devoir](images/capture-elea-B53/devoir_evaluation.png)

* Cacher une activité tout en la rendant disponible pour les élèves

    Il n'est plus possible de supprimer une section sans supprimer en même temps 
    les activités qui s'y trouvent. Il ne peut donc plus y avoir d'activité orpheline !
    
    Par contre, vous pouvez cacher une activité puis la rendre disponible. De nombreuses 
    utilisations possibles : masquer les activités à la vue des élèves tout en laissant 
    la seule carte de progression visible, masquer une base de données tout en en donnant
    un lien lorsqu'elle est utile dans le parcours, etc.

![sections orphelines](images/capture-elea-B53/activites_furtives.png)

* Attribuer des badges sur la base de l'acquisition d'autres badges

    Votre élève a acquis tous les badges de niveau 1 dans votre parcours ?
    Vous pouvez lui décerner automatiquement un badge supplémentaire de bonne maîtrise !
    
    **Dans votre parcours > Menu bleu > Administration du cours > Badges > Ajouter un badge**
