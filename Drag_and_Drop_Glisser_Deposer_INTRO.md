# Drag and drop (Glisser-Déposer)

<https://tube-numerique-educatif.apps.education.fr/w/9fr6i7YiRjNqyMVmHfonV9>

Dans ce contenu interactif l'élève disposera d'éléments qui peuvent être des images ou bien des textes ; éléments qu'il s'agira de glisser-déposer à la souris sur les endroits corrects prédéfinis au-dessus d'une image d'arrière-plan.

![Exemple d'activité de Drag and drop Glisser-Déposer](images/imagesdraganddropglisserdeposer/draganddropglisserdeposer01exemple.png)

Ici, par exemple, l'élève doit légender correctement le schéma en déplaçant les noms des huit planètes du système solaire. Ces étiquettes nominatives (de type "textes") peuvent être déposées sur des "zones de dépôt" qui ont été définies préalablement par l'enseignant (lequel contrôle ainsi par avance les endroits de l'image susceptibles ou non de recevoir un ou plusieurs des éléments à déplacer).

![Exemple d'activité de Drag and drop Glisser-Déposer](images/imagesdraganddropglisserdeposer/draganddropglisserdeposer02exemple.png)

Nous commencerons par créer un contenu interactif de type (en anglais) "**Drag and Drop**".

![Icône de l'activité Drag and drop Glisser-Déposer](images/imagesdraganddropglisserdeposer/draganddropglisserdeposericone.png)
