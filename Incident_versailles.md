# Incident du 28/11/2022

* L'attaque a repris le 29/11 dès 5h. Les mesures prisent hier ont permis que seule la plateforme du bassin de Versailles soit aujourd'hui affectée. La plateforme sera remise en ligne dès que possible.
* Tout est maintenant rentré dans l'ordre pour l'ensemble des plateformes ! Un nouveau dispositif a été mis en place dont nous espérons qu'il permettra de limiter les conséquences de toute nouvelle attaque à l'avenir.

La plateforme du bassin de Versailles a subi une attaque par déni de service (DDOS) tout au long de la journée.

**C'est la raison qui a causé les ralentissements extrêmes observés sur l'ensemble de l'infrastructure.**

L'attaque s'est arrêtée en fin de journée et des mesures ont été prises pour minimiser les désagréments si cela devait se reproduire.

**Aucun risque pour vos données dans ce type d'attaque.**

Toutes nos excuses pour les désagréments importants que vous avez dû supporter.
