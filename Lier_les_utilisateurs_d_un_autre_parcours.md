# Lier les utilisateurs d'un autre parcours

## Qu'est-ce que c'est ?

Vous avez deux parcours que vous voulez assigner aux mêmes utilisateurs. Vous pouvez lier ces deux parcours.

Le premier parcours, le parcours de référence définie les utilisateurs concernés. Cette liste forme ce que l'on pourrait appeler informellement un groupe (groupe d'élèves à besoins particuliers, groupe d'enseignants, etc).

En deux clics, le deuxième parcours se peut posséder exactement les mêmes utilisateurs.

## Comment ça marche ?

### Parcours de référence

Il s'agit d'un parcours avec une liste d'utilisateur bien définie. **Il n'y a aucune action particulière à faire dans ce parcours**. On va juste réutiliser ces utilisateurs qui peuvent être : 

- des cohortes,

  ![cohortes parcours de référence](images/lier_utilisateurs/parcours_reference_cohortes.png)

- des enseignants,

  ![enseignant parcours de référence](images/lier_utilisateurs/parcours_reference_enseignants.png)

- des elèves.


### Parcours lié

Vous possédez un autre parcours et vous voulez que les utilisateurs soient les mêmes. **Il suffit de cliquer sur l'icône de lien puis de choisir le parcours de référence.**

![lier les utilisateurs d'un parcours](images/lier_utilisateurs/lier_les_parcours.png)

Si vous regardez la liste des utilisateurs, on obtient la même liste :

![rajout utilisateurs](images/lier_utilisateurs/comparaison_liste_utilisateurs.png)



Vous pouvez même rajouter des utilisateurs (élèves, enseignants ou cohortes) à ce parcours particulier, cela ne change pas la liste des utilisateurs du parcours de référence.



Si vous modifiez les utilisateurs du parcours de référence, les utilisateurs du parcours lié sont automatiquement mis à jour.

![rajout utilisateurs](images/lier_utilisateurs/nouveaux_inscrits.png)





## Remarque : 

* Vous pouvez lier un parcours à plusieurs parcours de référence, cumulant ainsi les utilisateurs (par exemple, si vous avez deux groupes d'élèves à besoins particuliers, vous pouvez faire un parcours avec ces deux groupes d'utilisateurs).