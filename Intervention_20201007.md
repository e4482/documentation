# Intervention urgente du 07/10/2020

À partir de 19h une action indispensable sera exécutée sur les 28 plateformes Éléa.
Elles seront traitées l'une après l'autre pendant environ 10 minutes.

Il n'y a aucun risque pour les données des utilisateurs
mais **les plateformes seront inaccessibles** et **les utilisateurs déconnectés**.

Ci-dessous le planning de l'intervention.
Les horaires sont donnés à titre indicatif.
Vous pourrez suivre l'avancée du travail en direct :

| Plateforme | Horaire prévu | Statut |
| ------ | ------ | ------ |
| rambouillet | 19:00 | Terminé x2
| sqy | 19:10 | Terminé x2
| mantes | 19:20 | Terminé
| etampes | 19:30 | Terminé
| massy | 19:40 | Terminé x2
| gonesse | 19:50 | Terminé
| cergy | 20:00 | Terminé
| savigny | 20:10 | Terminé
| mureaux | 20:20 | Terminé
| argenteuil | 20:30 | Terminé
| enghien | 20:40 | Terminé
| pontoise | 20:50 | Terminé
| sarcelles | 21:00 | Terminé
| poissy | 21:10 | Terminé
| sgl | 21:20 | Terminé
| versailles | 21:30 | Terminé
| antony | 21:40 | Terminé
| boulogne | 21:50 | Terminé
| gennevilliers | 22:00 | Terminé
| nanterre | 22:10 | Terminé
| neuilly | 22:20 | Terminé
| vanves | 22:30 | Terminé
| evry | 22:40 | Terminé
| montgeron | 22:50 | Terminé
| communaute | 23:00 | Terminé