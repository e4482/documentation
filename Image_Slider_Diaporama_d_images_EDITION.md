## Créer la première image du diaporama

![Interface de création d'un diaporama d'images.](images/imageslideralbumimages/imageslideralbumimages01interfacealbumimages.png)

1. Donnez un titre à l'activité. C'est sous ce titre que cette activité s'affichera dans le parcours Éléa.
2. Écrivez le titre de la première image du diaporama. 
3. Cliquez sur "**+ Ajouter**". Puis parcourez les dossiers de votre ordinateur pour y sélectionner le fichier image souhaité. 

**Remarque** : en pratique il est conseillé d'avoir redimensionné auparavant toutes les images devant être incluses dans le diaporama, afin d'obtenir idéalement un format d'affichage (ou encore "ratio" d'affichage) similaire entre elles - voir plus bas "**Paramétrer le ratio d'affichage**".

4. Rédigez un **Texte alternatif**  : ce texte se substituera à l'image si celle-ci rencontre un quelconque problème d'affichage quand l'activité est consultée.
5. (optionnel) **Texte de survol** : rédigez ici un texte qui s'affichera sous forme de bulle volante quand le pointeur de la souris survolera l'image.

## Créer les images suivantes du diaporama

![Le bouton ajouter un item.](images/imageslideralbumimages/imageslideralbumimages02ajouteritem.png)

Cliquez sur "**Ajouter item**" autant de fois que souhaité pour ajouter une à une au diaporama chaque nouvelle image (et donc ajouter à chaque fois une page de plus à votre album). 

Recommencez les étapes précédentes 2 à 5 pour chaque nouvelle image ajoutée. 

Une image peut être supprimée (afin par exemple de lui en substituer une autre), en cliquant sur l'icone croix **x** en haut à droite.

![Icone suppression d'image](images/imageslideralbumimages/imageslideralbumimages02supprimerimage.png)

Votre album d'images est prêt : vous pouvez cliquer en bas de page sur "**Enregistrer et afficher**" pour le feuilleter.

![Le bouton enregistrer et afficher](images/imageslideralbumimages/imageslideralbumimages03boutonenregistreretafficher.png)

## Naviguer dans le diaporama d'images

![Un exemple de navigation dans un diaporama d'images.](images/imageslideralbumimages/imageslideralbumimages04outilsdenavigation.png)

Vous pouvez feuilleter le diaporama, page après page, grâce aux deux flèches blanches de part et d'autre de l'image affichée :

1. **<** image précédente.

2. et **>** image suivante.

3. À chaque image correspond aussi en bas de l'album un bouton rond : vous pouvez ainsi directement afficher l'image de votre choix en cliquant sur le bouton rond qui lui correspond.

4. Pour une navigation plus confortable, cliquez sur la double-flèche en haut à droite : le diaporama s'affiche alors en **plein écran**. 

Un appui sur la touche "**Échap**" (ou "**ESC**") de votre clavier vous permettra de sortir de ce mode **plein écran** et de revenir à l'affichage réduit.

![Repérer la touche Échap sur le clavier](images/imageslideralbumimages/imageslideralbumimages05topucheechap.png)

## Modifier le diaporama d'images

![Les boutons d'édition de le diaporama d'images.](images/imageslideralbumimages/imageslideralbumimages06outilseditionalbum.png)

1.Lors de la création du diaporama, pour votre confort de lecture à l'écran, il vous est possible de condenser tous les paramétrages d'une image donnée grâce à la flèche blanche **▼** qui est située dans la barre grise du titre de l'image. Ultérieurement, cliquez sur la flèche ► pour redéplier les paramétrages d'une image afin de pouvoir à nouveau les modifier.

2.Les images de le diaporama peuvent être déplacées entre elles (montées dans la liste en cliquant à droite sur **▲**, ou descendues en cliquant sur **▼**), ce qui modifiera leur ordre de défilement dans le diaporama. 

Cliquer sur la croix **x** permet de supprimer une image donnée.

## Modifier les images du diaporama

Un menu "**Éditer l'image**" apparaît pour chaque image ajoutée : il permet de rogner l'image ou de la pivoter quart de tour par quart de tour.

![Le bouton d'accès au menu d'édition des images.](images/imageslideralbumimages/imageslideralbumimages07boutonediterimage.png)

![Menu d'édition des images.](images/imageslideralbumimages/imageslideralbumimages08menueditionimages.png)

Enregistrez enfin les modifications appliquées à l'image.

![Le bouton pour enregistrer les modifications de l'image](images/imageslideralbumimages/imageslideralbumimages09enregistrerimage.png)

**Remarque** : un bouton copyright est aussi associé à chaque image du diaporama pour en renseigner les crédits et droits d'usage. Pour un rappel des principales licences en vigueur [cliquez ici](http://creativecommons.fr/licences/).

![Le bouton pour renseigner les crédits et droits d'usage des images qui apparaissent dans le diaporama.](images/imageslideralbumimages/imageslideralbumimages10boutoncopyright.png)

## Paramétrer le ratio d'affichage

Afin d'obtenir une présentation homogène quand le diaporama d'images sera feuilleté, il est conseillé d'avoir redimensionné au préalable toutes les images du diaporama dans un seul et même format.

![imageslideralbumimages11ratio](images/imageslideralbumimages/imageslideralbumimages11ratio.png)

Par défaut le **diaporama s'affiche toujours dans la plus grande largeur possible à l'écran** ; de là il existe trois options pour déterminer le ratio d'affichage du diaporama (c'est-à-dire la proportion entre la largeur et la longueur du diaporama).

- **Automatique** :  avec cette option le ratio d'affichage du diaporama sera le même que celui des images qui nécessitent la plus grande hauteur d'affichage. Des bordures noires seront automatiquement ajoutées sur le pourtour des images qui ont un ratio d'affichage différent.

- **Personnalisé** : en ce cas le ratio est défini par une largeur-type et une hauteur-type. 

**Exemples de ratios courants**.

Format carré 1:1.

![Exemple de ratio carré](images/imageslideralbumimages/imageslideralbumimages12ratiocarré.png)

Format quatre-tiers 4:3.

![exemple de ratio quatre tiers](images/imageslideralbumimages/imageslideralbumimages13ratioquatretiers.png)

Format seize-neuvièmes16:9.

![Exemple de ratio seize-neuvièmes](images/imageslideralbumimages/imageslideralbumimages14ratioseizeneuvièmes.png)

Là encore, des bordures noires seront automatiquement ajoutées sur le pourtour des images qui ont un ratio d'affichage différent de celui qui a été fixé pour le diaporama.

- **Variable** : à chaque nouvelle image affichée le diaporama adoptera alors le ratio adapté à celle-ci.

![Les options d'accessibilité](images/imageslideralbumimages/imageslideralbumimages15options.png)

Le menu "**Options et Textes**" permet enfin de modifier les libellés des boutons de navigation de l'activité. Ces libellés ne sont pas affichés, mais ce sont eux qui seront lus par les outils d'accessibilité améliorée par synthèse vocale.

(crédits images : vecteurs "quatre saisons" par macrovector pour freepik.com ; image clavier par pixabay.com)