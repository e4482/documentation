## Créer la consigne

![Interface d'édition de l'activité Image Sequencing Images par ordre logique](images/imagesequencingsequenceimages/imagesequencingsequenceimages02interface.png)

1. Donnez un titre à l'activité. C'est sous ce titre que cette activité s'affichera dans le parcours Éléa.

2. Rédigez la consigne qui spécifie l'ordre attendu pour le classement des images proposées.

**Exemple** : Cliquez-déplacez ces images pour les ordonner entre elles : vous devez recréer le cycle de vie de cette plante, de la germination à la pollinisation.

3. Rédigez à nouveau cette consigne : ce second libellé ne sera pas affiché, mais c'est lui qui sera lu par les outils d'accessibilité améliorée par synthèse vocale.

**Exemple** : Ordonnez ces images correctement. Utilisez les flèches du clavier pour naviguer parmi les images, utilisez la barre espace pour sélectionner ou désélectionner une image puis déplacez-la avec les flèches du clavier.

## Créer la première image de la séquence

![Interface d'édition d'une image dans l'activité Image Sequencing Images par ordre logique](images/imagesequencingsequenceimages/imagesequencingsequenceimages03interfaceimage.png)

1. Cliquez sur "**+ Ajouter**". Puis parcourez les dossiers de votre ordinateur pour y sélectionner le fichier image souhaité. 

**Remarque** : en pratique il est conseillé d'avoir redimensionné auparavant toutes les images devant être incluses dans la séquence, et ce afin d'obtenir un format d'affichage uniforme (idéalement un format quasi-carré de 165 pixels de large sur 160 pixels de haut est à privilégier).

2. Rédigez un **Texte alternatif**  : ce texte descriptif se substituera à l'image si celle-ci rencontre un quelconque problème d'affichage quand l'activité est consultée.

3. (optionnel) **Fichier Son** : cliquez sur le **+** puis parcourez les dossiers de votre ordinateur pour y sélectionner un fichier son qui sera associé à l'image en question. Ce fichier son peut, par exemple, permettre à l'élève d'écouter la prononciation d'un terme représenté dans l'image ; ou encore, il peut reprendre la description de celle-ci afin d'améliorer l'accessibilité de l'activité aux personnes déficientes visuelles.

![Icone effacer image](images/imagesequencingsequenceimages/imagesequencingsequenceimages13iconeeffacerimage.png)

Une image peut être supprimée (afin par exemple de lui en substituer une autre), en cliquant sur l'icone croix **x** en haut à droite.

## Compléter la séquence avec les images suivantes

![Barre de titres des images de la séquence](images/imagesequencingsequenceimages/imagesequencingsequenceimages04interfaceimage.png)

1.Pour ajouter chaque nouvelle image et en modifier les paramétrages, cliquez sur son titre dans la barre grisée à gauche. Recommencez pour chaque nouvelle image ajoutée les étapes précédentes ("**Ajouter**", "**Texte Alternatif**" et éventuellement ajout d'un fichier son). 

2.Par défaut la séquence prévoit d'inclure trois images : cliquez sur "**AJOUTER IMAGE**" pour ajouter une à une à la séquence autant de nouvelles images que souhaité. 

**Attention** : l'ordre, **de haut en bas**, dans lequel sont ajoutées les images (qui se retrouvent automatiquement numérotées 1, 2, 3 etc.) définit l'**ordre attendu comme étant la bonne réponse** à l'activité de classement de cette activité d'**Images par ordre logique**.

Autrement dit, au lancement de l'activité, cet ordre sera aléatoirement modifié pour l'affichage initial des images, et ce sera donc à l'élève de reconstituer cet ordre voulu.

Votre activité **Images par ordre logique** est prête : vous pouvez cliquer en bas de page sur "**Enregistrer et afficher**" pour la tester.

![Bouton Enregistrer et afficher](images/imagesequencingsequenceimages/imagesequencingsequenceimages05boutonenregistreretafficher.png)

## Modifier la séquence d'images

![Icones de déplacement et suppression des images de la séquence](images/imagesequencingsequenceimages/imagesequencingsequenceimages06deplacementsuppressionimages.png)

Les images de la séquence peuvent être déplacées entre elles : montées dans la liste en cliquant à gauche sur **▲**, ou descendues en cliquant sur **▼**. 

En conséquence ceci modifiera automatiquement l'ordre attendu comme étant la bonne réponse à l'activité.

Cliquer sur la croix **x** permet de supprimer une image donnée.

## Modifier les images de l'album

![Bouton éditer image](images/imagesequencingsequenceimages/imagesequencingsequenceimages07boutonediterimage.png)

Un menu "**Éditer l'image**" apparaît sous chaque image ajoutée : il permet de rogner cette image ou encore de la pivoter quart de tour par quart de tour.

![Menu d'édition des images](images/imagesequencingsequenceimages/imagesequencingsequenceimages08menueditionimages.png)

Enregistrez enfin les modifications appliquées à l'image.

![Le bouton pour enregistrer les modifications de l'image](images/imagesequencingsequenceimages/imagesequencingsequenceimages09enregistrerimage.png)

**Remarque** : un bouton copyright est aussi associé à chaque image de l'album pour en renseigner les crédits et droits d'usage. Pour un rappel des principales licences en vigueur [cliquez ici](http://creativecommons.fr/licences/).

![Bouton de copyright](images/imagesequencingsequenceimages/imagesequencingsequenceimages10boutoncopyright.png)

## Sélectionner les boutons et les options

![Interface des boutons et options](images/imagesequencingsequenceimages/imagesequencingsequenceimages11boutonsetoptions.png)

Cliquez sur la flèche ► pour dérouler le menu de gestion d'affichage des trois boutons disponibles pour l'activité : cochez la case correspondant aux boutons que vous souhaitez voir affichés pour l'élève.

1. Le bouton "**Solution**" : l'élève peut voir la solution de l'activité, c'est-à-dire les images correctement réordonnées entre elles. Cette option est à privilégier dans une optique d'auto-évaluation formative.

2. Le bouton "**Recommencer**" : une fois l'activité terminée l'élève peut lancer une nouvelle tentative.

3. Un bouton "**Reprendre**" sera proposé à l'élève à chaque fois qu'il lance une vérification de son travail : des coches rouges et vertes lui indiquent si les images sont convenablement placées ou non dans la séquence attendue.

![Exemple de correction intermédiaire](images/imagesequencingsequenceimages/imagesequencingsequenceimages12boutonrecommencer.png)

(Le menu déroulant "**Options et Textes**" permet enfin de modifier les libellés qui seront lus par les outils d'accessibilité améliorée par synthèse vocale).

(crédits images : vecteurs "Cycle de vie d'une plante" par brgfx pour freepik.com)