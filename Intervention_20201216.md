# Intervention du 16/12/2020

Entre 19h et 20h une action sera exécutée sur les 28 plateformes Éléa.

**À son tour, chaque plateforme sera inaccessible pendant quelques courtes minutes.**

Cette action permettra de rétablir les images des activités Appariement modifiées 
depuis les deux dernières mises à jour.