# Créer et utiliser des groupes

<https://tube-numerique-educatif.apps.education.fr/w/wMWhtiKK8MmevvCgVewPU9>

**Attention** : Désormais, des groupes correspondant aux différentes classes sont automatiquement créés à chaque inscription d'une classe à un parcours.

![inscription des classes](images/groupes/inscription_classes.png)

Par défaut, sur un parcours, il existe donc des groupes séparés correspondant à chacune des classes inscrites au parcours. 

![groupes classes par défaut](images/groupes/groupes_classes.png)

Vous pouvez filtrer les résultats des participants par groupe-classe 

![groupes classes par défaut](images/groupes/suivi_eleves.png)



Par conséquent, pour chaque activité du parcours, dans les réglages courants, l'option "groupe séparés" est activée par défaut. 

![groupes classes par défaut](images/groupes/groupes_separes.png)

Vous pouvez modifier cette option si vous le désirez

![groupes classes par défaut](images/groupes/groupes_separes2.png)


