# Déploiement de la version Bêta 67 le 05/12/2020

Une nouvelle mise à jour sera déployée sur l'ensemble des plateformes ce samedi 
tout au long de la journée.

**Toutes les plateformes seront inaccessibles de 13h à 17h !**

Les activités **Appariement** et **QCM** - qui sont spécifiques à Éléa - ont été réécrites 
pour permettre leur conversion à la demande en des activités **Test** natives de manière 
à ce que :
* L'enseignant puisse disposer des options beaucoup plus larges d'une telle activité 
lorsqu'il s'en sent prêt et sans qu'il ait à refaire son travail du début ;
* Le parcours puisse être partagé ou réutilisé hors de l'académie et donc d'Éléa 
si le professeur en a la volonté (on peut penser au cas des mutations inter-académiques).

> **Attention :** À l'issue de cette mise à jour, de très rares activités ne fonctionneront 
>plus et se comporteront comme suit :
> * pour le QCM un message,
>
> !["Activité en cours de conception" s'affiche](images/update/qcm.png)
>
> * pour l'appariement une activité sans aucune question.
>
> ![Seul le bouton "Vérifier la réponse" s'affiche](images/update/appariement.png)
>
> La situation perdurera pendant deux semaines le temps que la mise à jour suivante puisse 
>être provoquée. N'hésitez pas à nous contacter via le courriel support-elea@ac-versailles.fr 
>pour que nous puissions réparer manuellement votre parcours si vous ne pouvez pas patienter.

Ci-dessous l'ordre de traitement des plateformes.
Vous pourrez suivre l'avancée du travail en direct :

* **13h03** rafraichissement de la première moitié de l'infrastruture (pour l'instant les utilisateurs peuvent toujours utiliser Éléa grâce à la seconde moitié mais ça ne va durer qu'une demi-heure tout au plus),
* **13h32** verrouillage puis remise en ligne de la première moitité de l'infrastructure,
* **13h34** rafraichissement de la seconde moitié de l'infrastruture (les utilisateurs ne peuvent plus accéder aux plateformes),
* **14h06** mise à jour des plateformes (4 en parallèle),

| Plateforme    | Heure lancement | Heure succès |
|---------------|-------------------|--------|
| montgeron     | 14h06                  | 14h10       |
| evry          | 14h06                  | 14h23       |
| vanves        | 14h06                  | 14h11       |
| neuilly       | 14h06                  | 14h09       |
| nanterre      | 14h25                  | 14h28       |
| gennevilliers | 14h25                  | 14h32       |
| boulogne      | 14h25                  | 14h33       |
| antony        | 14h25                  | 14h32       |
| versailles    | 14h36                  | Incident       |
| sgl           | 14h36                  | 14h55       |
| poissy        | 14h36                  | 14h41       |
| sarcelles     | 14h36                  | 14h46       |
| pontoise      | 14h48                  | 14h53       |
| enghien       | 14h48                  | 14h54       |
| argenteuil    | 14h58                  | 15h01       |
| communaute    | 14h58                  | 15h19       |
| mureaux       | 14h58                  | 14h59       |
| gonesse       | 15h01                  | 15h09       |
| savigny       | 15h02                  | 15h17       |
| etampes       | 15h26                  | 15h42       |
| mantes        | 15h26                  | 15h37       |
| cergy         | 15h26                  | 15h33       |
| massy         | 15h39                  | 15h53       |
| formation     | 15h39                  | 15h41       |
| sqy           | 15h43                  | 16h21       |
| rambouillet   | 15h43                  | 15h50       |

**Incident :** La plateforme de Versailles rencontre un problème lié aux limites de nos serveurs de bases de données. Je suis en train de produire une nouvelle version de la mise à jour pour que la plateforme puisse être utilisée le temps de trouver une solution définitive.

* **18h31** réactivation de la plateforme de Versailles (la mise à jour de l'activité Appariement a été soustraite de la Bêta 67 de manière à passer l'éccueil en attendant une solution définitive).

> L'incident a été résolu le lundi 7 décembre entre 19h et 20h !
