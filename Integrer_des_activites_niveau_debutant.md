# Intégrer une activité Test

## Créer des questions et définir les paramètres généraux

<https://tube-numerique-educatif.apps.education.fr/w/vFtsqwgNTa2FqY6LNvrZuV>

## Paramétrer des feedbacks (en fonction des réponses)

<https://tube-numerique-educatif.apps.education.fr/w/8NfqwNLsagUmhFFpqknxeC>

## Récupérer les résultats des élèves et corriger leurs réponses

<https://tube-numerique-educatif.apps.education.fr/w/unhti6tyTpMjy5KdupXUmj>

# Intégrer une activité QCM

<https://tube-numerique-educatif.apps.education.fr/w/9d3gukvYV7qLTE9D8BrQL5>

# Intégrer une activité Glossaire

<https://tube-numerique-educatif.apps.education.fr/w/t6C2xX6j39fW8ru9BFqTYq>

# Intégrer une activité Leçon

## La leçon thématique
<https://tube-numerique-educatif.apps.education.fr/w/2oLQ5TNSqDUD1uz4KQAuVF>

## La leçon différenciée
<https://tube-numerique-educatif.apps.education.fr/w/qzN815oajGy8CMWBNfKS7a>

# Intégrer une activité Base de données

## Tutoriel pour le professeur
<https://tube-numerique-educatif.apps.education.fr/w/31ztoHtqbyrsCxiPYgH1Go>

## Tutoriel pour les élèves
<https://tube-numerique-educatif.apps.education.fr/w/qp4iAk1WGqXQzXT9zUQi3V>
