# Créer un connecteur Éléa sur e-Collège

## Accéder à la console

Connectez-vous à l'ENT avec un compte administrateur. Si vous l'êtes bien, vous trouverez l'icône __Administration__ dans vos applications pour lancer la console.

![admin-icon](images/ent78/bouton.png)

## Créer un connecteur

1. Cliquez sur __Gérer les connecteurs__.

  ![console](images/ent78/console.png)

2. Puis en haut à droite sur __Créer un connecteur__.

   ![connecteur](images/ent78/connecteur.png)

## Paramétrer votre connecteur

Utilisez le tableau et la capture d'écran ci-dessous pour compléter les champs.

| Paramètre       | Valeur                                                       |
| --------------- | ------------------------------------------------------------ |
| URL de l'icône  | https://sgl.elea.ac-versailles.fr/theme/vital/pix/logo.png |
| Identifiant     | __elea__ suivi de votre __UAI/RNE__ (sans espace, sans majuscule) |
| Nom d'affichage | Éléa                                                         |
| URL             | https://sgl.elea.ac-versailles.fr/login/index.php?authCAS=DEPT78 |
| Cible           | Nouvelle page                                                |

<span style="color: #FF0000">__ATTENTION__ : Ces deux liens sont valables uniquement pour les établissements du bassin de Saint-Germain en Laye. Vous devez les adapter à votre situation en vous aidant du tableau en annexe !</span>



![admin-cas-params](images/ent78/parametres.png)

En dessous, dans "Champs spécifiques CAS", activez le champs spécifique CAS, puis choisir comme type Eléa.

![admin-cas-params](images/ent78/parametres2.png)

Cliquez enfin sur le bouton **Créer**.

![Bouton Créer](images/ent91/boutoncreerconnecteur.png)

Remarques : 
- Il existe un temps de latence pendant lequel l'erreur INVALID_SERVICE peut apparaître.
- L'administrateur doit se déconnecter de l'ENT avant de tester le connecteur.
- Si vous apportez ultérieurement des modifications sur cette page, cliquez alors en haut à droite sur __Enregistrer__.

![save](images/ent78/save.png)

## Activer les droits

Vous devez maintenant décider qui pourra accéder à ce connecteur. Rendez-vous dans **Attribution**.

![droits](images/ent78/droits.png)

Vous devriez donner l'accès aux profils suivants :

* Tous les enseignants
* Tous les élèves
* Tous les personnels

*Les parents n'ont pas accès à Éléa puisqu'ils n'ont pas vocation à créer des parcours et qu'il n'est pas attendu qu'ils réalisent les exercices ;-)*

## Annexe - adresse URL de chaque bassin

| Nom       | Adresse                                 |
| --------- | --------------------------------------- |
| Poissy   | https://poissy.elea.ac-versailles.fr   |
| Mantes     | https://mantes.elea.ac-versailles.fr      |
| Mureaux     | https://mureaux.elea.ac-versailles.fr      |
| Rambouillet     | https://rambouillet.elea.ac-versailles.fr      |
| Saint-Quentin en Yvelines    | https://sqy.elea.ac-versailles.fr     |
| Saint-Germain en Laye | https://sgl.elea.ac-versailles.fr |
| Versailles  | https://versailles.elea.ac-versailles.fr   |
