# Créer un établissement sur Eléa

Cette procédure est nécessaire pour créer un établissement sur les plateformes Eléa.
En tant que conseiller(-ère) de bassin, vous disposez d'un compte local spécifique sur la plateforme dont le nom d'utilisateur est « manager ».
Pour créer votre mot de passe, utilisez la procédure d'oubli de mot de passe en précisant ce nom (« manager ») pour recevoir un lien par courriel.

## Étape 1

Entrez l'url correspondant à votre plateforme de bassin sur le modèle **BASSIN.elea.ac-versailles.fr**.
Cliquez sur **Démarrer**.

![page_accueil_elea](images/accueil_elea/capture1.png)

Cliquez sur **« Utiliser mon compte local »**.

![choix_compte](images/accueil_elea/capture2.png)

Puis entrez l'identifiant **« manager »** et le mot de passe.

![connexion_manager](images/accueil_elea/capture3.png)

## Étape 2

Les établissements déjà existants sur la plateforme sont listés par commune.

![liste_etablissements](images/platform_agent/capture4.png)

Cliquez sur **« Ajouter un nouvel établissement »**.

![ajouter_etablissement](images/platform_agent/capture5.png)

Complétez **l'UAI de l'établissement** que vous souhaitez ajouter, puis cliquez sur **« Rechercher »**.

![UAI_etablissement](images/platform_agent/capture6.png)

Les informations relatives à l'établissement sont pré-saisies.
Cliquez sur **« Valider »** si elles sont correctes. Les modifications restent exceptionnelles.

![validation_infos](images/platform_agent/capture7.png)

Un message vous informe que l'établissement a été ajouté avec succès. 
Vous pouvez continuer à ajouter des établissements ou fermer la fenêtre si vous souhaitez terminer la procédure.

![fin_ajout](images/platform_agent/capture8.png)

La liste des établissements est mise à jour automatiquement lorsque vous quittez l'assistant.

![liste_etablissements](images/platform_agent/capture9.png)

**IMPORTANT** : **Il faut vérifier le contenu de la colonne CAS. S'il n'est pas fait mention d'un ENT alors que l'établissement en possède un ou bien que la solution ENT mentionnée n'est pas la bonne, écrire au support.
Seuls les administrateurs pourront procéder à la correction.**

## Étape 3

A l'issue de la création d'un établissement, un courriel automatique est envoyé au chef d'établissement ou à l'IEN de circonscription, l'informant qu'il peut désormais administrer son espace sur Eléa.
Il pourra à ce moment-là **créer les comptes utilisateurs** ou déléguer cette opération au référent Eléa.

Si le chef d'établissement a perdu les identifiants de ce compte local, vous pouvez lui communiquer la documentation correspondante, le tutoriel **« Tuto PERDIR mdp compte local »**. Le nom d'utilisateur correspond à l'UAI de l'établissement.

Si malgré cela des difficultés persistent, vous pouvez adresser un courriel au support Eléa **support-elea@ac-versailles.fr** en l'accompagnant impérativement d'une ou plusieurs captures d'écran faisant apparaître le problème.

