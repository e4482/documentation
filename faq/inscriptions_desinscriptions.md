#Inscriptions / Désincriptions
##Un élève est arrivé en cours d’année et ne figure pas dans la liste d’élèves de la classe : que faire ?

Lorsqu'un élève arrive en cours d'année, il est nécessaire de faire un nouvel import des comptes utilisateurs de votre établissement.

C'est une manipulation qui est réalisée par le référent e-éducation/Éléa de votre établissement.

Vous pouvez vous rapprocher de lui et éventuellement lui transmettre le [lien suivant](https://communaute.elea.ac-versailles.fr/local/faq/index.php?role=ref) afin qu'il puisse consulter le tutoriel de votre département. 

##Comment inscrire une classe à un parcours ?

Suivez le [tutoriel](https://scolawebtv.crdp-versailles.fr/?id=44863) pour partager votre parcours avec vos élèves.

##Comment inscrire à un parcours Éléa un groupe d'élèves issus de classes différentes ?

Suivez le [tutoriel](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=gerer-les-utilisateurs&item=inscrire-a-un-parcours-elea-un-groupe-d-eleves-issus-de-classes-differentes) pour partager votre parcours avec vos élèves.

##Je travaille sur plusieurs établissements de l'académie de Versailles : comment partager mes parcours avec des élèves issus de ces différents établissements ?

Demandez d'abord à chaque référent Eléa de vos établissements si l'importation des utilisateurs a été réalisée ou réactualisée.

Le dernier import des comptes réalisé dans l'un ou l'autre de vos établissements vient se placer toujours en "tête" de la liste répertoriée sur la plateforme Éléa des établissements où vous exercez , au risque d'écraser l'établissement qui se trouvait là précédemment.


Ouvrez votre plateforme de bassin d'Eléa via l'ENT.

Cliquez d'abord sur le **Menu latéral (l'icône bleue à trois bandes blanches en haut à droite)** ; puis sur l'icône **Mon profil** et ensuite **Modifier le profil**.

Cliquez sur l'onglet **Etablissements** pour visualiser et inscrire un RNE par ligne.

Laissez le champ  RNE1 vide (car c'est celui-ci qui, parce qu'il est en tête de liste, sera systématiquement écrasé à chaque nouvel import : autant donc le laisser vide).

En revanche, **saisissez les RNE (appelés désormais UAI) de vos différents établissements dans RNE2, RNE3, etc...**  (attention n'employez ici qu'une minuscule pour la lettre finale incluse dans ces RNE/UAI).

Validez vos modifications en cliquant sur **Enregistrer le profil**.

