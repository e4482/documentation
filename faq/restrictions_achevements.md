# Restrictions d'accès et achèvements des activités

## Comment rendre disponibles les activités et les ressources les unes après les autres ?

Vous souhaitez que vos élèves aient réussi une activité avant de faire la suivante. Pour cela, il faut ajouter des conditions **d'achèvement** pour chacune des activités et ressources. Ensuite **activez le mode édition**, puis dans la **barre d'édition** en haut de la page, cliquez sur l'onglet **Restrictions** puis **Rendre les activités disponibles l'une après l'autre**. Vous aurez des restrictions d'achèvement sur l'activité précédente.
La démarche vous est détaillée dans ce [tutoriel](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=concevoir-des-parcours&item=decouvrir-la-barre-d-edition-d-un-parcours).


## Comment conditionner l'accès aux ressources et activités dans un parcours ?

"Pour chaque activité et ressource l'enseignant peut :
1. Au terme de l'activité paramétrer une condition d'**achèvement** de l'activité : c'est-à-dire définir sous quelle condition on considèrera que l'élève a terminé le travail attendu.
2. À l'entrée de l'activité paramétrer une **restriction d'accès** de l'activité : c'est-à-dire définir sous quelle condition l'élève peut consulter une activité, généralement cette restriction sera liée à une date prédéfinie, à une note obtenue dans une activité effectuée précédemment dans le parcours, ou bien au fait que l'élève appartient à un groupe d'élèves donné, etc".
La démarche vous est détaillée dans ce [tutoriel](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=concevoir-des-parcours&item=parametrer-le-suivi-d-activite-des-eleves).

## Comment faire pour que les élèves accèdent à la suite du parcours, avec une note minimale à une activité ? 
Vous souhaitez ajouter des restrictions d'accès à vos élèves. Nous vous invitons à consulter les deux tutoriels dédiés à ce sujet.
La démarche vous est détaillée dans ce [tutoriel](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=integrer-des-activites&item=parametrer-des-activites-suivi-d-achevement-et-restrictions-d-acces).

## Comment planifier l'accès à une partie d’un parcours à une date donnée ?
Vous souhaitez ajouter des restrictions d'accès pour vos élèves. Nous vous invitons à consulter [les deux tutoriels dédiés à ce sujet](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=integrer-des-activites&item=parametrer-des-activites-suivi-d-achevement-et-restrictions-d-acces).

## Comment paramétrer les horaires d'un Chat ? 
**Attention** : les paramétrages de date et d'horaire de la section "**Sessions de Chat**" sont purement **informatifs** mais pas restrictifs. Autrement dit si l'activité Chat est laissée visible, les élèves ont accès à ce tchat en dehors de la date et de l'horaire mentionnés dans "**Sessions de Chat**".
Pour limiter effectivement l'accès au Chat vous devrez comme pour toute autre activité définir des **restrictions d'accès** par date.
Nous vous invitons à consulter [les deux tutoriels dédiés à ce sujet](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=integrer-des-activites&item=parametrer-des-activites-suivi-d-achevement-et-restrictions-d-acces).

## Est-il possible d'empêcher un élève de réaliser le parcours plusieurs fois ?
Il est possible de mettre une restriction par date aux activités du parcours pour éviter que les élèves ne le réalisent après une certaine date. Si les activités sont liées entre elles, vous devez bloquer l'accès à la première activité. Pour les activités **Test**, il est possible de limiter le nombre de tentatives mais ce n'est pas le cas de toutes les activités. 

## Comment paramétrer les conditions d'achèvement et les restrictions d'accès d'un module H5P ?

Vous pouvez paramétrer les restrictions d'accès et d'achèvement une ressource interactive (H5P) comme toute autre activité, en lui ajoutant notamment des restrictions d'accès.

## J'ai un problème technique avec les modules H5P, les élèves ne peuvent pas accéder à l'activité suivante : comment faire ?
Le problème est lié au fonctionnement particulier du plugin H5P : il ne rafraîchit jamais automatiquement la page de l'utilisateur entre deux actions de celui-ci (on appelle cela l'AJAX et, Moodle en utilise encore très peu). La flèche en bas à droite ne changera de couleur que si la page du navigateur est rechargée.