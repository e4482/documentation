# Sections et éléments
## Comment organiser un parcours en sections ?

Pour diviser votre parcours en plusieurs parties, vous pouvez ajouter des sections. Pour ce faire, lorsque vous êtes dans votre parcours avec le mode édition activé, cliquez sur le bouton **ajouter des sections** en bas à droite de l'écran. Vous pourrez ensuite les renommer si besoin.


## Comment cacher une section mais rendre l’activité tout de même disponible ?

Vous pouvez décider de cacher une ou plusieurs sections de votre parcours. Les élèves navigueront ainsi dans votre parcours grâce aux flèches directionnelles ou bien en suivant une carte de progression. Pour ce faire, vous devez cacher la section, et rendre ses activités disponibles (pour que les élèves y aient accès). Nous vous invitons à consulter ce [tutoriel](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=concevoir-des-parcours&item=cacher-les-sections-sur-la-page-d-accueil).

## À quoi sert une étiquette ? 

Vous pouvez insérer une étiquette dans la section 0 de votre parcours, afin d'inclure un texte de présentation par exemple. Nous vous invitons à consulter ce [tutoriel](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=integrer-des-ressources&item=integrer-une-ressource-etiquette).

## Quelle activité choisir pour faire compléter un texte à trous par les élèves ? 

Vous pouvez réaliser une activité texte à trous dans une activité Test ou avec le contenu interactif. Nous vous invitons à consulter le [tutoriel de l'activité Test](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=integrer-des-activites&item=integrer-une-activite-test) et [celui du contenu interactif](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=integrer-des-contenus-interactifs&item=fill-in-the-blanks-texte-a-quot-trous-quot).

## Comment réorganiser les activités de mon parcours ?

Vous pouvez réorganiser les activités de votre parcours selon votre scénario pédagogique. Nous vous invitons à consulter ce [tutoriel](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=concevoir-des-parcours&item=deplacer-une-activite).