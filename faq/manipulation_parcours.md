# Manipulation de parcours
## Comment enregistrer le travail réalisé sur mon parcours ? 

Sur chaque activité/ressource créée, vous **enregistrez** le contenu ajouté en cliquant sur l'un des deux boutons en bas de page **enregistrer et afficher** ou **enregistrer et revenir au cours**. Il n'y a pas d'autre manipulation à faire pour sauvegarder votre travail. Votre parcours s'enregistre automatiquement. Vous retrouverez votre parcours sur la plateforme où vous l'avez créé.

## Comment sauvegarder l'intégralité d'un parcours ? 

Vous pouvez sauvegarder votre parcours directement sur la plateforme. Pour cela, vous pouvez consulter le tutoriel. Vous aurez ainsi la possibilité de télécharger le fichier de votre parcours.

Pour ce faire, vous trouverez la marche à suivre dans ce [tutoriel](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=dupliquer-un-parcours).

## Comment récupérer un parcours de la Éléathèque pour l'utiliser avec ses élèves ?

Un parcours de la Éléathèque vous intéresse ?

Vous pouvez récupérer tout le parcours ou seulement les parties qui vous intéressent. Pour ce faire, vous trouverez la marche à suivre dans ce [tutoriel]( https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=utiliser-la-eleatheque&item=decouvrir-et-recuperer-des-parcours ).

## Comment dupliquer un parcours ? 

Pour dupliquer un parcours complet, vous devez tout d'abord procéder à la sauvegarde de ce parcours, puis à sa restauration dans un autre parcours. Lors de la sauvegarde, vous avez le choix de sauvegarder le parcours complet ou bien seulement certaines activités. Cette sauvegarde prend la forme d'un fichier de type archive compressée (fichier de type **.mbz** équivalent d'un fichier .zip). Ce fichier est hébergé par défaut sur votre plateforme Éléa ou peut être sauvegardé sur votre ordinateur personnel. Vous pourrez ensuite restaurer ce fichier de sauvegarde au format **.mbz** dans un autre parcours de votre choix en cliquant sur le menu latéral, puis "**Administration du cours**" et enfin "**Restaurer**". 

La démarche vous est détaillée dans ce [tutoriel](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=dupliquer-un-parcours).

## Comment copier une activité/ressource d'un parcours vers un autre ?

Importez le parcours qui contient les activités et ressources que vous souhaitez déplacer dans le parcours dans lequel ces activités et ressources doivent être dupliquées. Au moment de l'importation vous pourrez cocher les activités et ressources du parcours d'origine à dupliquer et décocher les autres (qui ne seront ainsi pas importées). 

La démarche vous est détaillée dans ce [tutoriel](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=faire-migrer-un-parcours).

## Comment partager un parcours avec un collègue de mon établissement ? 

Vous souhaitez travailler à plusieurs sur la conception d'un parcours ? Pour cela, vous pouvez inscrire votre (vos) collègue(s) en tant qu'enseignant à votre parcours. Consultez le [tutoriel vidéo](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=concevoir-des-parcours&item=inscrire-des-collegues). 

Si vous souhaitez partager un parcours avec un collègue, sans lui laisser les droits d'édition, vous devrez l'inscrire avec un rôle d'enseignant non éditeur. Enfin, si votre collègue veut apporter des modifications qui ne correspondent pas à votre scénario, nous vous conseillons de créer un fichier de sauvegarde et le partager, afin que le collègue le restaure dans un de ses propres parcours.

## Comment partager un parcours avec un collègue qui travaille en dehors de l'académie de Versailles, ou lui donner l'accès à mes parcours ?

Il n'est pas possible de donner l'accès aux parcours hébergés sur la plateforme Éléa à des collègues travaillant en dehors de l'académie de Versailles. Il est toutefois possible de partager une sauvegarde du parcours sous forme de fichier compressé afin que ces collègues le restaurent sur une installation tierce de type Moodle.

La démarche vous est détaillée dans ce [tutoriel](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=dupliquer-un-parcours).

## Comment partager un parcours avec un collègue qui travaille dans un autre bassin d'éducation que le mien dans l'académie de Versailles ?

Inscrivez ce collègue à votre parcours avec ses identifiants académiques (en ...@ac-versailles.fr). Attention : ce collègue doit avoir déjà effectué une première connexion à sa propre plateforme de bassin avec ses identifiants académiques pour ensuite apparaître dans le répertoire des utilisateurs d'Éléa avec ce profil académique.

La démarche vous est détaillée dans ce [tutoriel](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=concevoir-des-parcours&item=inscrire-des-collegues).

## Comment utiliser le même glossaire dans des parcours différents ?

Cliquez sur l'activité Glossaire, puis sur le menu latéral (icône bleue à trois bandes blanches en haut à droite) « Administration du Glossaire ». Cliquez ensuite sur "Sauvegarder" et "Passer à la dernière étape". Sur la page finale apparaîtra en tête de liste un fichier à l'extension .mbz que vous aurez la possibilité de télécharger sur votre ordinateur personnel. Il sera possible ultérieurement dans un autre parcours de créer de suivre le même chemin en cliquant sur "Restaurer" (et de là choisir le fichier de sauvegarde du glossaire qui apparaît dans la liste ou déposer ce même fichier que vous avez sauvegardé précédemment sur votre ordinateur). En optant pour les options Export/Import il est possible de suivre la même procédure pour fusionner des glossaires entre eux. 

## Le glossaire de mon parcours dupliqué est vide : comment faire ? 

En effet, lors de la duplication d'un parcours, le glossaire ne se duplique pas. Vous devez dans votre premier glossaire cliquer sur le menu latéral (icône bleue à trois bandes blanches en haut à droite) « **Administration du glossaire** », puis "**Exporter des articles**". Vous allez pouvoir enregistrer votre glossaire. Puis, depuis le glossaire vide du deuxième parcours, cliquez sur le menu latéral (icône bleue à trois bandes blanches en haut à droite) « **Administration du glossaire**", puis "**Importer des articles**". Vous pouvez déposer ici le fichier de sauvegarde de votre glossaire initial.

## Comment organiser le rangement de ses parcours ?

Vous pouvez ranger vos parcours en créant des dossiers thématiques. Vous pouvez également ajouter des sous-dossiers sur votre tableau de bord. Vous devez d'abord ajouter un nouveau dossier puis le déplacer dans un dossier existant comme indiqué dans ce tutoriel :

![Icone déplacement de parcours](imagesFAQ/sousdossierseleateacherboard01.png)

![Arborescence des parcours](imagesFAQ/sousdossierseleacapture02.png)

![Repli sous-dossier](imagesFAQ/sousdossierseleacteacherboard03.png)

## Puis-je modifier un parcours alors que des élèves sont déjà inscrits ? 

Même si vos élèves sont inscrits au parcours, vous pouvez y apporter des modifications. Il faut cependant prendre garde aux activités déjà effectuées par les élèves, notamment celles qui impactent la carte de progression et la navigation. 

## Deux collègues ont travaillé sur deux parcours distincts et veulent les fusionner dans un seul et unique parcours. Est-il possible de copier une partie d'un parcours A et de le coller dans un parcours B ?

Vous pouvez importer tout ou partie d'un parcours dans un autre. Les deux parcours doivent être sur la même plateforme (ou tous les deux hébergés sur la plateforme académique Communauté). Par précaution, il est recommandé de dupliquer indépendamment chacun de ces parcours avant de procéder à leur fusion. L'enseignant qui souhaite procéder à la fusion doit être inscrit avec le rôle "**Enseignant éditeur**" dans les deux parcours. Dans un des parcours cliquez sur le menu latéral, puis "**Administration du cours**" et enfin "**Importer**". Vous pourrez alors lancer une recherche sur l'intitulé du parcours qui est à importer, le sélectionner et enfin l'importer tout ou partie. Il sera possible ensuite de modifier l'ordre des activités, d’en rajouter, d'en supprimer.

La démarche vous est détaillée dans ce [tutoriel](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=faire-migrer-un-parcours).

## Est-il possible de copier ses parcours sur une autre plateforme de type Moodle ?

Cette manipulation est possible en passant par une sauvegarde et une restauration du parcours. Cependant, il convient de faire attention à : 

- la version de la plateforme moodle que vous utiliserez par rapport à la version moodle de la plateforme Éléa ;
- les trois activités du mode simple **Jeu du millionnaire**, **QCM**, **Appariement** et les cartes de progression sont des modules développés par l'académie de Versailles.

Vous pourrez retrouver toutes les informations sur les mises à jour de la plateforme Éléa [sur l'article dédié](http://www.dane.ac-versailles.fr/etre-accompagne-se-former/elea-mises-a-jour).

## Est-il possible d'importer un QCM de Pronote vers la plateforme Eléa ?

Vous pouvez importer un QCM créé sur Pronote vers votre parcours Eléa. Vous devez exporter de Pronote ce QCM au format **XML**. Puis, vous devez cliquer sur le menu latéral (icône bleue à trois bandes blanches en haut à droite) « **Administration du cours**», puis sur « **Banque de questions**" et enfin "**Importer**". Choisissez ensuite le format **XML Moodle** avant de déposer le fichier.
