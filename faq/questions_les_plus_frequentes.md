# Questions les plus fréquentes
## Je me suis désinscrit de mon parcours par inadvertance : comment le récupérer ?
Pas d'inquiétude. Une mise à jour automatique cette nuit va vous réattribuer votre parcours. Si la suppression de votre parcours est plus ancienne, écrivez avec votre mail académique au [support-elea@ac-versailles.fr](mailto:support-elea@ac-versailles.fr).


## J'ai supprimé une activité ou une ressource par inadvertance : comment la récupérer ? 
Pas d'inquiétude. Vous pourrez retrouver les activités supprimées dans la corbeille. Elles sont disponibles pendant quelques jours, après elles seront définitivement supprimées. 

Comment faire ? 

Vous venez de supprimer votre activité ou ressource. Il va falloir attendre un moment (environ 30 minutes) afin que celles-ci apparaissent dans la corbeille. Ensuite, il faut cliquer sur le menu bleu en haut à droite, puis sur **Administration du cours**, puis sur **Corbeille**. Vous retrouverez vos activités/ressources récemment supprimées. Pour qu'elles réapparaissent dans le parcours, il faut cliquer sur **Restauration**.

## J’ai accidentellement supprimé un parcours : comment le récupérer ?
Nous préférons vous prévenir qu'il est très difficile de retrouver votre parcours mais vous pouvez toujours envoyer un mail au [support-elea@ac-versailles.fr](mailto:support-elea@ac-versailles.fr) en précisant :

- le nom de votre parcours ;
- l'heure et la date de suppression de votre parcours ;
- le bassin de votre établissement ;
- et son **UAI**. 