# Nom de la catégorie comme on l'afficherait (ce n'est pas le cas actuellement)
## Question 1
## Question 2 qui a la même réponse que la première

Ceci est un élément de réponse.

Un autre

![Bouton H5P](../images/creercontenusinteractifspartiecommune/creercontenuinteractif01activiteh5p.png)

C'est une réponse vraiment très longue

## Une autre question.

Une autre réponse.

> avec une citation

et un tableau

| Titre 1       |     Titre 2     |        Titre 3 |
| :------------ | :-------------: | -------------: |
| Colonne       |     Colonne     |        Colonne |
| Alignée à     |   Alignée au    |      Alignée à |
| Gauche        |     Centre      |         Droite |