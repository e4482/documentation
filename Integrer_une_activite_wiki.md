# Intégrer une activité Wiki

<https://tube-numerique-educatif.apps.education.fr/w/93544mz7joux67tpGYMxo2>

Remarque : 

- Si vous souhaitez ajouter une autre page à votre wiki, indiquez le nom de celle-ci entre double crochet [[...]] sur la première page créée.

![ajouter une page à un wiki](images/activite_wiki/activite_wiki1.png)

- Puis enregistrez et afficher

![paramétrer une nouvelle page à un wiki](images/activite_wiki/activite_wiki2.png)

- Cliquez sur le nom de la page suivante (qui apparaît en rouge), une nouvelle fenêtre s'ouvre permettant la création d'une nouvelle page.

![créer une nouvelle page](images/activite_wiki/activite_wiki3.png)

- Vous pouvez désormais renseigner votre nouvelle page
![renseigner la nouvelle page à un wiki](images/activite_wiki/activite_wiki4.png)


Vous pouvez ajouter autant de pages que vous le souhaitez.

