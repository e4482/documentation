# Que faire si l'import des comptes échoue ?

Il est possible que l'opération d'import des utilisateurs de la plateforme Éléa se bloque à l'étape 2. 

Le premier conseil à suivre est de réaliser cet import en **privilégiant** les **navigateurs "Firefox" ou "Chrome"**.

Le problème survient généralement lorsque la structure de l'établissement comporte un nombre important de classes et de groupes. En effet, une des causes les plus probables est que votre navigateur envoie des requêtes au serveur de façon trop rapide, et que celui-ci n'arrive plus à suivre.

La solution présentée ici consiste alors à ralentir artificiellement la vitesse de votre connexion internet dans votre navigateur.

## Voici la démarche à suivre en fonction du navigateur :

- **Avec le navigateur Firefox :**

Sur la page d'importation, appuyer simultanément sur les touches de votre clavier **Ctrl + Maj + E**

Une fenêtre proposant des outils de développement s'ouvre dans la partie basse du navigateur, directement dans le menu **Réseau**.

![firefox-dev-web](images/Tuto_ralentir_web/firefox-dev-web.png)

Dans la partie droite de cette zone, un sous-menu indique "*Aucune limitation de la bande passante*".

![firefox-dev-web2](images/Tuto_ralentir_web/firefox-dev-web2.png)

Cliquer sur ce menu, puis choisir une connexion **Regular 2G** ou **Regular 3G**.

![firefox-dev-web3](images/Tuto_ralentir_web/firefox-dev-web3.png)

Retenter l'opération d'import avec ce paramétrage. Vous pourrez suivre l'envoi des requêtes dans la fenêtre.

Une fois l'import réalisé, n'oubliez pas de revenir aux paramètres par défaut avant de quitter la fenêtre.



- **Avec le navigateur Chrome :**

Appuyer simultanément sur les touches **Ctrl + Maj + I**

Une fenêtre apparait dans la partie droite du navigateur

![chrome-dev1](images/Tuto_ralentir_web/chrome-dev1.png)

Cliquez sur **Network**, puis sur le menu "**No throttling**"

![chrome-dev2](images/Tuto_ralentir_web/chrome-dev2.png)

Choisir la vitesse "**slow 3G**"

Voici une capture reprenant ces opérations si votre menu est en français.

![chrome-dev3](images/Tuto_ralentir_web/chrome-dev3.png)

Retenter l'opération d'import avec ce paramétrage.

Une fois l'import réalisé, n'oubliez pas de revenir aux paramètres par défaut avant de quitter la fenêtre pour débrider votre vitesse de connexion.

