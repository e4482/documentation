# Intégrer une activité Test

## Créer des questions et définir les paramètres généraux

<https://tube-numerique-educatif.apps.education.fr/w/vFtsqwgNTa2FqY6LNvrZuV>

## Paramétrer des feedbacks (en fonction des réponses)

<https://tube-numerique-educatif.apps.education.fr/w/8NfqwNLsagUmhFFpqknxeC>

## Récupérer les résultats des élèves et corriger leurs réponses

<https://tube-numerique-educatif.apps.education.fr/w/unhti6tyTpMjy5KdupXUmj>
