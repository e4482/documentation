# Utiliser une fonction LaTeX

<iframe title="e-éducation &amp; Éléa - Utiliser LaTeX pour écrire des formules mathématiques dans un parcours" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/vzxwF3yzJn7HAZSdfiwS2D" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

<https://tube-numerique-educatif.apps.education.fr/w/vzxwF3yzJn7HAZSdfiwS2D>
