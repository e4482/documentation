## Créer la première carte de révision

![Interface de l'activité Flashcards Cartes de révision](images/imagesflashcardscartesflash/flashcardscartesflash02interface.png)

1. Donnez un titre à l'activité. C'est sous ce titre que cette activité s'affichera dans le parcours Éléa.

2. Rédigez ici la consigne générale pour les élèves : cette consigne doit s'appliquer à toutes les **Cartes de révision** de la série proposée. 

**Exemple** : "Nommez en un seul mot ces figures de géométrie."

3. **Question** : rédigez la question qui sera associé à la première **Carte de révision**.

4. **Réponse** : rédigez ici la réponse attendue. 

**Attention** : Dans ce type de contenu interactif, les questions à réponses courtes sont à privilégier. L'enseignant devra ainsi éviter toute question ambiguë pouvant accepter de multiples réponses correctes. En effet la réponse de l'élève ne sera comparée qu'à **la seule formulation exacte** renseignée ici par l'enseignant.

**Exemple** : cette **Carte de révision** conviendrait ici puisque un seul mot correct est attendu : "triangle" (et à l'exception de tout triangle singulier : isocèle ou autre).

![Exemple d'une Carte de révision correcte](images/imagesflashcardscartesflash/flashcardscartesflash03exemplecartecorrecte.png)

En revanche cette **Carte de révision** est à éviter puisqu'elle serait susceptible de recevoir de multiples réponses correctes (carré, losange, quadrilatère, parallélogramme, rectangle). 

![Exemple d'une Carte de révision incorrecte](images/imagesflashcardscartesflash/flashcardscartesflash04exemplecarteincorrecte.png)

5. **Image** : Cliquez sur le bouton "**+ Ajouter**". Puis parcourez les dossiers de votre ordinateur pour y sélectionner le fichier image souhaité qui sera associé à la Question/Réponse précédemment rédigée.

Pour supprimer une image (en vue de lui en substituer une autre par exemple) cliquez sur la croix **x** en haut à droite de celle-ci.

![Icône de suppression d'image](images/imagesflashcardscartesflash/flashcardscartesflash05suppressionimage.png)

6. Rédigez ici un **Texte alternatif**  : ce texte se substituera à l'image si celle-ci rencontre un quelconque problème d'affichage quand l'activité est consultée.

7. **Indice** (optionnel) : vous pouvez éventuellement rédiger un court indice pour guider l'élève. Celui-ci verra apparaître une icone ![Icône indice](images/imagesflashcardscartesflash/flashcardscartesflash06indice.png)qui lui permettra d'un clic de lire l'indice proposé.

8. Afin d'éviter une consultation "passive" de la série de **Cartes de révision**, on pourra, en cochant cette case, contraindre l'élève à fournir au moins une réponse avant de pouvoir consulter la réponse correcte attendue.

9. En cochant cette case, la **casse** des caractères (majuscule/minuscule) sera prise en compte dans la correction.

Votre première **Carte de révision** est prête : vous pouvez cliquer en bas de page sur "**ENREGISTRER ET AFFICHER**" pour la tester.

![Bouton enregistrer et afficher](images/imagesflashcardscartesflash/flashcardscartesflash07boutonenregistreretafficher.png)

## Créer et organiser les cartes de révision suivantes

![Interface de navigation des Cartes de révision](images/imagesflashcardscartesflash/flashcardscartesflash07interfacenavigationcartes.png)

1. Cliquez sur "**+ AJOUTER CARTE**" autant de fois que nécessaire pour ajouter à la série autant de **Cartes de révision** supplémentaires que souhaité. Pour chaque nouvelle carte, on recommencera les opérations précédentes (rédaction de la question et de la réponse, et ajout de l'image associée).
2. Pour naviguer entre les différentes cartes, cliquez sur le titre de la carte à modifier dans la barre grisée à gauche. Vous pouvez aussi modifier l'ordre des cartes dans la série en cliquant sur les flèches ▲ et ▼ pour monter ou descendre une carte par rapport aux autres. Cliquez sur la croix **x** pour supprimer définitivement une carte.

## Éditer les images (optionnel)

Sous chaque image ajoutée à la série de **Cartes de révision**, un menu **Éditer l'image** apparaît : il permet de rogner l'image ou de la pivoter quart de tour par quart de tour.

![Bouton éditer l'image](images/imagesflashcardscartesflash/flashcardscartesflash08boutonediterimage.png)


![Menu d'édition d'image](images/imagesflashcardscartesflash/flashcardscartesflash09menueditionimages.png)

Enregistrez enfin les modifications appliquées à l'image.

![Bouton d'enregistrement d'une image](images/imagesflashcardscartesflash/flashcardscartesflash10enregistrerimage.png)

**Remarque** : un bouton copyright est aussi associé à chaque image de l'album pour en renseigner les crédits et droits d'usage. Pour un rappel des principales licences en vigueur [cliquez ici](http://creativecommons.fr/licences/).

![Le bouton pour renseigner les crédits et droits d'usage des images qui apparaissent dans l'album.](images/imagesflashcardscartesflash/flashcardscartesflash11boutoncopyright.png)

Enfin**,** le menu "Options et Textes" offre la possibilité de modifier les libellés des textes et boutons de l'activité, les mentions affichées en cas de bonne ou mauvaise réponse, ou encore les textes qui seront lus par un assistant de synthèse vocale pour une meilleure accessibilité de l'activité. Les mentions @score et @total sont des mentions génériques auxquelles se substitueront respectivement le score effectivement obtenu par l'élève pour une tentative donnée, et le nombre total de cartes de la série.