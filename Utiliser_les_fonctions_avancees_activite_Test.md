# Utiliser une fonction LaTeX

<https://tube-numerique-educatif.apps.education.fr/w/vzxwF3yzJn7HAZSdfiwS2D>

# Intégrer des questions aléatoires

<https://tube-numerique-educatif.apps.education.fr/w/kNTTZdUCCLnjbewXLgozvA>

# Intégrer des questions calculées

<https://tube-numerique-educatif.apps.education.fr/w/d7upTEqJAegR3cUVTa93dB>

# Permettre à l'élève de s'enregistrer

<https://tube-numerique-educatif.apps.education.fr/w/oBchb7fN7Htpej1vV4KTYk>
