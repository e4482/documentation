# Intégrer une activité Jeu d'appariement 

L'activité **Jeu d'appariement** permet de créer un jeu d'association d'éléments par paires. Cela peut être des éléments textuels ou multimédias, formules mathématiques.

Cette activité est une activité d'entraînement, donc les élèves pourront la tenter autant de fois qu'ils le souhaitent.

## Étape 1 - Ajouter une activité Jeu d'appariement

Pour créer une activité **Jeu d'appariement**, ouvrez le parcours sur lequel vous souhaitez ajouter cette activité. 

Cliquez en haut à droite sur le bouton "**Activer le mode édition**".

Dans la barre d'édition tout en haut, cliquez sur le menu "**Activités**", puis sur l'icône correspondante à l'activité Jeu d'appariement.

![Appariement- Choix de l'activité](images/activiteappariement/Capture1.png)

Ou sélectionnez la section concernée puis :

- Cliquez sur **"Ajouter une activité ou une ressource"** ;

- Cliquez dans l'onglet **"Activité"**

- Puis, sélectionnez l'activité **Jeu d'appariement **dans la liste ;

![Appariement- Choix de l'activité](images/activiteappariement/Capture2.png)

Une nouvelle page s'ouvre pour définir les paramètres de l'activité. 

## Étape 2 - Ajouter du contenu dans votre activité Jeu d'appariement

Commencez par attribuer un nom à votre activité et, si vous le souhaitez, une petite description.

![Appariement-titre](images/activiteappariement/Capture3.png)

- **Paramétrez** paire à paire les éléments qui doivent être associés. Les éléments côte à côte seront associés dans l’exercice d’appariement. Vous pouvez insérer des images en y associant une description pour les logiciels d’accessibilité, des ressources multimédias sons et vidéos (que vous pouvez déposer sur Peertube ou PodEduc d'Apps éducation s’ils sont volumineux). 

![Appariement-Choix de l'activité](images/activiteappariement/Capture4.png)

Vous pouvez ajouter jusqu'à 10 paires à votre jeu.

![Appariement-Choix de l'activité](images/activiteappariement/Capture5.png)

- **Enregistrez** votre exercice. Les éléments peuvent alors être associés. 
- Il est enfin possible de **paramétrer** votre jeu d'appariement (restrictions d'accès, achèvement d'activités...) : [Paramétrer des activités : suivi d'achèvement et restrictions d'accès ](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=concevoir-des-parcours&item=parametrer-des-activites-et-ressources-suivi-d-achevement-et-restrictions-d-acces).

## Étape 3 - Visualiser votre activité jeu d'appariement

Vous aurez la possibilité de visualiser votre **Jeu d'appariement**, en cliquant sur chaque élément de la colonne de gauche, vous devez ensuite cliquer sur l'élément correspondant dans la colonne de droite. Les éléments s'associent en glissant vers le haut. Cliquez sur le bouton "Vérifier la réponse" pour valider votre réponse.

En cas de bonnes réponses, des coches apparaissent.

![Appariement-Choix de l'activité](images/activiteappariement/Capture6.png)

En cas de mauvaises réponses, des croix apparaissent.

![Appariement-Choix de l'activité](images/activiteappariement/Capture7.png)

En cliquant sur le fil d'Ariane pour revenir au parcours, nous pouvons constater que l'activité s'est créée à la suite des activités précédentes.

Il est possible de modifier le titre de l'activité en cliquant sur le crayon, ou en allant sur **Modifier**, **Paramètres**. Vous pourrez alors modifier le contenu si besoin, ou modifier les réglages courants.

Enregistrez l'activité en cliquant sur **Enregistrer et afficher** pour la visionner, ou en cliquant sur **Enregistrer et revenir au cours** pour revenir au cours.

## Étape 4 - Suivre l'activité des élèves

Plusieurs cas de figure dans le tableau de suivi :

![Modifier le jeu](images/activitemillionaire/Ts.png)

- L'élève n'a pas réalisé l'activité : par défaut, la case correspondante à l'activité est de couleur grise avec un rond noir et le chiffre 0 indique que l'élève n'a effectué aucune tentative.

![Modifier le jeu](images/activitemillionaire/KO.png)

- L'élève a réalisé au moins une tentative : par défaut, l'activité est considérée comme achevée. La case s'affiche en vert et le nombre dans le cercle indique le nombre de tentative(s).

![Modifier le jeu](images/activitemillionaire/OK2.png)

- Si vous souhaitez savoir si l'activité a été réussie (100% de réussite), vous devez ne laisser cocher dans les conditions d'achèvement que la case "L'étudiant doit recevoir une note pour achever cette activité".

![Modifier le jeu](images/activitemillionaire/Capture8.png)

Vous pourrez alors vérifier que l'activité a été réussie ou non après "x" tentative(s) grâce à la couleur de la case. Par exemple, dans le cas ou l'élève aurait tenté l'activité 1 fois en commettant des erreurs, la case restera grise avec le chiffre 1 à l'intérieur. En cas de réussite, la case passe au vert.

![Modifier le jeu](images/activitemillionaire/KO1.png)

