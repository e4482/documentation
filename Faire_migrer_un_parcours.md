# Faire migrer un parcours sur une autre plateforme

J'ai créé un parcours sur ma plateforme de bassin. Je souhaite : 
- le transférer sur communaute.elea pour travailler avec d'autres collègues.
- le transférer sur une plateforme pour le proposer à mes élèves, car j'ai obtenu une mutation.


## Etape 1 : Sur la plateforme où se trouve le parcours
__Prérequis__ : Avoir les droits d'enseignants sur le parcours à migrer

Connectez-vous à la plateforme Eléa où se trouve le parcours à migrer. Ouvrez le parcours. 
Cliquez sur le bouton bleu en haut à droite pour ouvrir le menu 
![admin-icon](images/migrer_un_parcours/capture0.png)


Dans le menu, sélectionnez "Administration du cours" 
![admin-icon](images/migrer_un_parcours/capture1.png)


Pour finir, sélectionnez "sauvegarde" 
![admin-icon](images/migrer_un_parcours/capture2.png)

Il s'agit maintenant de choisir les données qui seront sauvegardées. Une fois les éléments inclus, cliquez sur "suivant"  
Remarque : Il n'est pas possible actuellement d'inclure les badges. Il conviendra de les rajouter manuellement.


Il vous est également possible de ne sauvegarder qu'une partie du parcours. Pour cela, il vous suffit de décocher les éléments pour qu'ils ne figurent pas dans la sauvegarde.
![admin-icon](images/migrer_un_parcours/capture2a.png)

Cliquez sur "suivant" puis "Effectuer la sauvegarde" et pour finir "Continuer"

Dans la zone de sauvegarde, vous pouvez télécharger votre fichier de sauvegarde sur votre ordinateur. Gardez le précieusement. Il vous servira pour l'étape 2.
 ![admin-icon](images/migrer_un_parcours/capture2aa.png)


## Etape 2 : Sur la plateforme où vous souhaitez migrer le parcours

Créer un nouveau parcours en utilisant **impérativement** le gabarit "parcours vide" !

![admin-icon](images/migrer_un_parcours/parcours_vide.png)

![admin-icon](images/migrer_un_parcours/parcours_vide2.png)

Rendez-vous dans ce nouveau parcours

Cliquez sur le bouton bleu en haut à droite pour ouvrir le menu 
![admin-icon](images/migrer_un_parcours/capture0.png)

Sélectionnez Administration du cours" 
![admin-icon](images/migrer_un_parcours/capture1.png)

puis restaurer
![admin-icon](images/migrer_un_parcours/capture3.png)

Importez le fichier de sauvegarde de votre parcours. Vous avez deux possibilités :

- Cliquez sur le bouton "Choisir un fichier" et sélectionnez dans votre ordinateur la sauvegarde que vous venez de faire de votre parcours.
![admin-icon](images/migrer_un_parcours/capture4.png)
- Faire un glisser votre fichier de sauvegarde dans la zone à cet effet
![admin-icon](images/migrer_un_parcours/capture5.png)

Il ne vous reste plus qu'à cliquer sur "Restaurer".
![admin-icon](images/migrer_un_parcours/capture6.png)

Cliquez en bas à droite sur "Continuer".

Cochez **impérativement** "Fusionner le cours sauvegardé avec ce cours". Cliquez sur "Continuer".

![admin-icon](images/migrer_un_parcours/capture7.png)

Lors de cette phase de la restauration, il vous est possible de :
- renommer votre cours, 
- de définir une date de début du cours
- de conserver les groupes

![admin-icon](images/migrer_un_parcours/capture8.png)

- De restaurer qu'une partie du parcours sauvegardé

![admin-icon](images/migrer_un_parcours/capture9.png)

Cliquez sur "Effectuer la restauration" puis sur "Continuer" à 2 reprises.



Attention : si le parcours modèle comprend des badges, il convient de les [ajouter](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=concevoir-des-parcours&item=ajouter-un-badge "tutoriel pour ajouter un badge") manuellement.







