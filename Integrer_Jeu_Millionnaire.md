# Intégrer une activité Jeu du Millionnaire

L'activité **Jeu du Millionnaire** permet de créer un jeu de questions sur le principe du célèbre jeu télévisé.

Cette activité est une activité d'entraînement, donc les élèves pourront la tenter autant de fois qu'ils le souhaitent.

## Étape 1 - Ajouter une activité Jeu du millionnaire

 Pour créer une activité **Jeu du Millionnaire**, ouvrez le parcours sur lequel vous souhaitez ajouter cette activité. 

Cliquez en haut à droite sur le bouton "**Activer le mode édition**".

Dans la barre d'édition tout en haut, cliquez sur le menu "**Activités**", puis sur l'icône correspondante à l'activité Jeu du Millionnaire.

![Choix de l'activité](images/activitemillionaire/Capture1.png)

Ou sélectionnez la section concernée puis :

- Cliquez sur **"Ajouter une activité ou une ressource"** ;

- Cliquez dans l'onglet **"Activité"**

- Puis, sélectionnez l'activité **Jeu du Millionnaire **dans la liste ;

![Appariement- Choix de l'activité](images/activitemillionaire/Capture2.png)

Une nouvelle page s'ouvre pour définir les paramètres de l'activité. 

## Étape 2 - Ajouter du contenu dans votre activité Jeu du millionnaire

Complétez les informations classiques comme le titre de votre activité et sa description.

![Titre et description du jeu](images/activitemillionaire/Capture3.png)

Complétez ensuite les différentes questions, ainsi que les propositions de réponses. 

![Interface de création des questions](images/activitemillionaire/Capture4.png)

Un certain nombre de points est attribué à chaque question, et le niveau de difficulté des questions est croissant.

Une fois celles-ci complétées, vous pouvez régler les derniers paramètres du jeu et cliquez sur **Enregistrer et afficher**. 

![Réglages](images/activitemillionaire/activitemillionaire04reglages.png)

## Étape 3 - Visualiser votre activité jeu du millionnaire

Vous aurez la possibilité de visualiser votre **Jeu du Millionnaire**, avec la première question et les propositions de réponses.

![Aperçu des questions](images/activitemillionaire/Capture5.png)

Lorsque vous cliquez sur une bonne réponse, vous passez à la question suivante.

![Une bonne réponse](images/activitemillionaire/Capture6.png)

En cas d'erreur,  vous devrez recommencer l'exercice depuis le début.

![Réponse fausse](images/activitemillionaire/Capture7.png)

En cliquant sur le fil d'Ariane pour revenir au parcours, nous pouvons constater que l'activité s'est créée à la suite des activités précédentes.

Il est possible de modifier le titre de l'activité en cliquant sur le crayon, ou en allant sur **Modifier**, **Paramètres**. Vous pourrez alors modifier le contenu si besoin, ou modifier les réglages courants.

![Modifier le jeu](images/activitemillionaire/activitemillionaire08renommer.png)

Enregistrez l'activité en cliquant sur **Enregistrer et afficher** pour la visionner, ou en cliquant sur **Enregistrer et revenir au cours** pour revenir au cours.

## Étape 4 - Suivre l'activité des élèves

Plusieurs cas de figure dans le tableau de suivi :

![Modifier le jeu](images/activitemillionaire/Ts.png)

- L'élève n'a pas réalisé l'activité : par défaut, la case correspondante à l'activité est de couleur grise avec un rond noir et le chiffre 0 indique que l'élève n'a effectué aucune tentative.

![Modifier le jeu](images/activitemillionaire/KO.png)

- L'élève a réalisé au moins une tentative : par défaut, l'activité est considérée comme achevée. La case s'affiche en vert et le nombre dans le cercle indique le nombre de tentative(s).

![Modifier le jeu](images/activitemillionaire/OK2.png)

- Si vous souhaitez savoir si l'activité a été réussie (100% de réussite), vous devez ne laisser cocher dans les conditions d'achèvement que la case "L'étudiant doit recevoir une note pour achever cette activité".

![Modifier le jeu](images/activitemillionaire/Capture8.png)

Vous pourrez alors vérifier que l'activité a été réussie ou non après "x" tentative(s) grâce à la couleur de la case. Par exemple, dans le cas ou l'élève aurait tenté l'activité 1 fois sans aller au bout, la case restera grise avec le chiffre 1 à l'intérieur. En cas de réussite, la case passe au vert.

![Modifier le jeu](images/activitemillionaire/KO1.png)
