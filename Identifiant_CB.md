Si vous êtes conseiller de bassin, votre nom d'utilisateur est **"manager"** (suivi éventuellement d'un 2 dans le cas où vous n'êtes pas le seul dans votre bassin).

Un mail contenant un lien permettant la réinitialisation du mot de passe sera envoyé sur votre adresse mail.

**Attention** : Ce lien n'est valable que pendant **30 minutes**.

![mail_lien_reinitialisation](images\accueil_elea\mail_lien_reinitialisation.png)

1. Cliquez sur le lien. 

2. Une nouvelle page s'ouvre, cliquez sur **"CONTINUER"** 

![continuer](images\accueil_elea\mdp_oubli2.png)

3. Définissez votre nouveau mot de passe puis cliquez sur **"ENREGISTRER"**

![réinitialisation du mot de passe](images\accueil_elea\mdp_oubli3.png)

Vous pouvez de nouveau vous connecter.