# Intégrer une activité Sondage

<https://tube-numerique-educatif.apps.education.fr/w/ntGZcWfBPHzmVGh5zfk26N>

## Étape 1 - Ajouter une activité Sondage

Pour créer une activité **Sondage**, ouvrez le parcours sur lequel vous souhaitez ajouter cette activité. 

Cliquez en haut à droite sur le bouton "**Activer le mode édition**".

![Sondage-Choix de l'activité](images/activitesondage/activitesondage01activerlemodeedition.png)

Sélectionnez ensuite la section concernée et :

- Cliquez sur **"Ajouter une activité ou une ressource" **;

![Sondage-Choix de l'activité](images/activitesondage/activitesondage02ajouteruneactiviteouressource.png)

- Passez en **"Mode Avancé" **;

![Sondage-Choix de l'activité](images/activitesondage/activitesondage03modeavance.png)

- Sélectionnez l'activité **Sondage **dans la liste ;

![Sondage-Choix de l'activité](images/activitesondage/activitesondage04choisiractivitesondage.png)

- Et enfin cliquez sur **"Ajouter"**.

Une nouvelle page s'ouvre pour définir les paramètres de l'activité. 

## Étape 2 - Ajouter du contenu dans votre activité Sondage

Commencez par attribuer un nom à votre activité et si vous le souhaitez une petite description.

![Sondage-Titre](images/activitesondage/activitesondage05ajoutsondageparamètres.png)

**Paramétrez** les **options** de votre sondage. Vous avez la possibilité de :
- permettre ou non, la modification du choix ;
- permettre ou non, le choix de plusieurs réponses ;
- limiter ou non, le nombre de réponses permises (lorsque le sondage est utilisé pour permettre aux élèves de se répartir en groupes par exemple).

![Sondage- Options](images/activitesondage/activitesondage06optionssondage.png)

Vous pouvez ensuite remplir les options du sondage (en indiquant si nécessaire la limite de réponses permises par option).

![Sondage- Options](images/activitesondage/activitesondage07optionssondagechoix.png)

## Étape 3 - Paramétrer les résultats de votre activité Sondage

- **Paramétrez** les **résultats** de votre sondage. 
  Faites votre choix concernant : 

  - la publication des résultats ;
  ![Sondage- Options](images/activitesondage/activitesondage08résultatssondage.png)

  - la confidentialité des résultats.
  ![Sondage- Options](images/activitesondage/activitesondage09resultatssondageconfidentialitee.png)


- **Enregistrez** votre sondage. Il est alors prêt à être utilisé.

- Il est enfin possible de **paramétrer** votre jeu d'appariement (restrictions d'accès, achèvement d'activités...) : [Paramétrer des activités : suivi d'achèvement et restrictions d'accès ](?role=prof&element=integrer-des-activites&item=parametrer-des-activites-suivi-d-achevement-et-restrictions-d-acces).
