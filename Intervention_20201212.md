# Déploiement de la version Bêta 68 le 12/12/2020

Une nouvelle mise à jour sera déployée sur l'ensemble des plateformes ce samedi 
entre 13h et 14h30.

**À son tour, chaque plateforme ne sera inaccessible que pendant 5 minutes tout au plus.**

Cette mise à jour était initialement programmée pour le courant de la semaine prochaine. 
Sa mise en place avancée permettra de corriger un bug agaçant qui bloque actuellement 
les élèves dans les parcours faisant usage d'un achèvement et d'une restriction sur note 
de passage minimale après un QCM.

Au-delà, elle permettra de réparer les quelques activités QCM et Appariement qui ne 
pouvaient bénéficier du nouveau format de ces activités dès la Bêta 67 (limite liée aux 
serveurs actuels de bases de données). 
[Détails ici.](https://communaute.elea.ac-versailles.fr/local/faq/?role=update&element=beta-67)
