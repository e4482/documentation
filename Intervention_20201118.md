# Intervention du 18/11/2020 et jours suivants

Une action d'optimisation des données doit être exécutée sur les 28 plateformes Éléa.
Elles seront traitées l'une après l'autre **entre 5h15 et 7h45 au fil des jours**.

Il n'y a aucun risque pour les données des utilisateurs qui seront préalablement sauvegardées
mais **les plateformes seront inaccessibles** et **les utilisateurs déconnectés**.

**L'intervention n'affectera pas la disponibilité des plateformes sur le temps de classe !**

Ci-dessous l'ordre de traitement des plateformes.
Vous pourrez suivre l'avancée du travail en direct :

| Plateforme    | Horaire lancement | Statut |
|---------------|-------------------|--------|
| formation     | 18/11 05:15                  | Terminé       |
| mureaux       | 18/11 05:21                  | Terminé       |
| argenteuil    | 18/11 05:30                  | Terminé       |
| gennevilliers | 18/11 05:36                  | Terminé       |
| nanterre      | 18/11 05:41                  | Terminé       |
| gonesse       | 18/11 05:45                  | Terminé       |
| montgeron     | 18/11 05:50                  | Terminé       |
| poissy        | 18/11 05:55                  | Terminé       |
| neuilly       | 18/11 06:01                  | Terminé       |
| enghien       | 18/11 06:06                  | Terminé       |
| sarcelles     | 18/11 06:11                  | Terminé       |
| mantes        | 18/11 06:19                  | Terminé       |
| versailles    | 18/11 06:25                  | Terminé       |
| rambouillet   | 18/11 06:34                  | Terminé       |
| vanves        | 18/11 06:43                  | Terminé       |
| antony        | 18/11 06:50                  | Terminé       |
| cergy         | 18/11 06:55                  | Terminé       |
| pontoise      | 18/11 07:01                  | Terminé       |
| savigny       | 18/11 07:07                  | Terminé       |
| etampes       | 18/11 07:14                  | Terminé       |
| boulogne      | 18/11 07:25                  | Terminé       |
| massy         | 19/11 06:15                  | Terminé       |
| sqy           | 19/11 06:31                  | Terminé       |
| sgl           | 19/11 06:45                  | Terminé       |
| evry          | 19/11 07:04                  | Terminé       |
| communaute    | 19/11 07:17                  | Terminé       |