# Image Slider (Diaporama d'images)

Ce contenu interactif (H5P) permet de créer un diaporama d'images que l'élève pourra feuilleter image après image (à raison d'une page par image).

Grâce à ce contenu très polyvalent un enseignant pourra ainsi présenter de manière attractive et agréable à consulter une série de documents à étudier, des travaux d'élèves qu'il aura préalablement scannés, des schémas, un protocole d'expérience à suivre étape par étape, des corrigés d'exercices, etc.

Nous commencerons par créer un contenu interactif de type (en anglais) "**Image Slider**".

![Icone de l'activité Image Slider (diaporama d'images)](images/imageslideralbumimages/imageslideralbumimagesicone.png)